# shortcuts to various useful commands to setup dev envs

setup-ansible-deps:
	ansible-galaxy install --roles-path ansible/roles -r ansible/requirements.yml --force

vault_edit_prod:
	ansible-vault edit --vault-password-file .ansible_vault_pwd ansible/group_vars/production/vault.yml

vault_edit_staging:
	ansible-vault edit --vault-password-file .ansible_vault_pwd ansible/group_vars/staging/vault.yml
