# FAQ

La documentation détaillée est accessible via les onglets en haut de page (ou le menu :material-menu: sur un téléphone).

## À quoi sert Ma Dada ?

Ma Dada est une plateforme qui vous aide à réaliser des demandes d’accès à des documents publics, dits “administratifs”.

La procédure est relativement simple, puisqu’elle se déroule en deux étapes.

Premièrement, vous devez choisir l'autorité publique à laquelle vous souhaitez faire une demande de documents administratifs.

Deuxièmement, vous devez écrire votre demande (à l’aide si besoin de notre formulaire pré-rempli).

Ma Dada envoie votre demande en votre nom, mais en cachant votre adresse email. Vous recevrez un email lorsque l'autorité aura répondu ou lorsque le délai de réponse sera expiré (un mois).

Toutes les réponses reçues sont automatiquement publiées sur le site, afin qu’elles profitent au plus grand nombre.

## Que puis-je demander ?

Par documents administratifs, la [loi](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000033218936) vise tous “les documents produits ou reçus, dans le cadre de leur mission de service public, par l'Etat, les collectivités territoriales ainsi que par les autres personnes de droit public ou les personnes de droit privé chargées d'une telle mission”. Cela vaut “quels que soient leur date, leur lieu de conservation, leur forme et leur support”.

Concrètement, cela concerne notamment :

- les décisions (arrêtés, délibérations, etc.)
- les documents de marchés publics (factures, offres de prix des candidats, procès-verbal d’ouverture des plis, etc.)
- les statistiques et autres données
- les correspondances (courriers, emails…)
- les autorisations d’urbanisme (dossiers de permis de construire, etc.)
- les comptes rendus (de conseil municipal, de réunions, etc.)
- les codes sources
- les photos et vidéos
- les rapports et avis d’experts

Tous ces documents administratifs sont « communicables » de droit, par principe, à moins qu’ils ne soient couverts par un secret protégé par la loi : secret défense, secrets des affaires, protection de la vie privée, etc.

## Y a-t-il des choses que je ne peux pas demander ?

Rien n’interdit de demander. Cependant, l’administration peut refuser de vous communiquer des documents, notamment si leur consultation porterait atteinte :

- Au secret de la défense nationale
- A la recherche et à la prévention, par les services compétents, d'infractions de toute nature
- Au secret des délibérations du Gouvernement
- A la sécurité publique ou à la sécurité des personnes
- A la monnaie et au crédit public
- A la sécurité des systèmes d'information des administrations

Certains documents ne peuvent être communiqués qu’à certaines personnes, par exemple dans le cas d’un dossier médical. Ce type de document ne doit d’ailleurs pas être sollicité via Ma Dada pour cette raison.

Si cela est possible, l’administration doit simplement occulter les mentions problématiques. Cela vaut tout particulièrement pour protéger les données à caractère personnel.

## A qui puis-je demander des documents administratifs ?

Le droit d’accès s’applique à tous les organismes - et même les entreprises - dès lors qu’ils sont chargés d’une mission de service public.

Mairies, ministères, services déconcentrés, départements, autorités administratives indépendantes (CNIL, AMF…), communautés d’agglomérations, CAF… sont par exemple concernés.

Vous pouvez également faire valoir ce droit auprès de certaines entreprises, à l’image de la SNCF ou d’Enedis par exemple, dès lors que les documents sollicités ont été produits ou reçus dans le cadre d’une mission de service public.

Attention cependant : l’Assemblée nationale et le Sénat ne sont pas concernés par le droit d’accès aux documents administratifs (au nom de la séparation des pouvoirs).

## Dois-je expliquer pourquoi je souhaite obtenir un document ?

Non, ce n’est pas obligatoire. Dans certains cas, cela peut cependant contribuer à un meilleur traitement de votre demande.

## Est-ce que cela va me coûter quelque chose ?

Non, cela est totalement gratuit. Seule exception : si vous sollicitez des copies occasionnant des frais de reproduction pour l’administration (s’il faut faire des photocopies par exemple). Voilà pourquoi il est généralement plus judicieux de demander des documents au format électronique.

## Comment m’y prendre pour faire ma demande ?

Première étape : vous devez sélectionner l’autorité à laquelle vous souhaitez envoyer votre demande.

Deuxième étape : rédiger votre demande.

Notre plateforme vous propose un texte pré-rempli, dans lequel vous n’avez qu’à ajouter le ou les documents que vous souhaitez obtenir.

Vous pouvez cependant modifier ce texte à votre guise, au besoin en utilisant les quelques modèles que nous avons préparés pour vous aider dans certains cas de figures :

- [Documents d’urbanisme (permis de construire,
  PLU…)](https://forum.madada.fr/t/tutoriel-obtenir-des-documents-d-urbanisme-permis-de-construire-plu-etc/985)
- [Documents de marchés publics](https://forum.madada.fr/t/tutoriel-obtenir-des-documents-relatifs-a-un-marche-public/969)
- [Documents préalables à une décision publique](https://forum.madada.fr/t/tutoriel-savoir-comment-a-ete-prise-une-decision-publique-grace-au-droit-d-acces-aux-documents-administratifs/960)
- [Notes de frais](https://blog.madada.fr/articles/2023/02/14/notes-de-frais.html)

Il ne vous reste plus qu’à cliquer sur Envoyer !

## Comment augmenter ses chances de réussite ?

C’est surtout en facilitant le travail de l’administration que l’on peut augmenter ses chances de réussite. Dès lors, quatre règles d’or :

- Envoyer la demande à l’administration qui a produit le document (ou éventuellement l’a reçu)
- Identifier le plus précisément possible le ou les documents sollicités (un rapport, des statistiques, une délibération, un code source, etc.)
- Préciser la période concernée, le cas échéant (les données de l’année 2020, la délibération du 12 juillet 1998, etc.).
- S’assurer que le ou les documents demandés soient “définitifs”, c’est-à-dire qu’il ne s’agisse pas de brouillons ou que la décision qu’ils sont censés influencer ait été prise (marché public conclu, décret paru au Journal officiel, etc.).

## Que se passe-t-il une fois que ma demande a été envoyée ?

L’administration dispose d’un délai d’un mois pour vous répondre.

Si vous avez obtenu le document sollicité, bravo, l’aventure s’arrête là !

Si l’administration vous a refusé l’accès au document, ou n’a pas répondu durant le délai d’un mois, vous avez la possibilité de saisir la Commission d’accès aux documents administratifs (CADA). Cette autorité indépendante fait alors office de médiateur entre l’administration et le demandeur. Bon à savoir : ce recours est gratuit, et peut désormais s’effectuer depuis Ma Dada.

## Pourquoi mon nom et ma demande apparaissent publiquement sur Ma Dada ?

Nous publions votre demande sur Internet, afin que tout le monde puisse utiliser les informations que vous avez reçues : si un document vous intéresse, il est probable qu’il puisse intéresser d’autres personnes !

Votre nom est publié sur Ma Dada car l’administration est [en droit d’exiger cette information](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000033288123) pour traiter votre demande. Nous travaillons à réduire la visibilité des noms de personnes tout en permettant de traiter les demandes.

Notre plateforme n’interdit pas l’utilisation de pseudonymes, mais cela peut nuire au succès de votre demande d’accès. Si vous ne souhaitez pas que votre nom apparaisse sur Ma Dada, vous pouvez nous demander de procéder à une occultation de celui-ci par mail, à contact@madada.fr.

## Qui est derrière Ma Dada ?

Ma Dada est une initiative portée par l’association [Open Knowledge
France](https://fr.okfn.org/notre-groupe-local/gouvernance/).

Deux personnes composent actuellement l'équipe de MaDada : Xavier (délégué général) et Laurent (pour la technique). La plateforme est maintenue quotidiennement, parfois avec l’aide de membres de l’association, voire d’utilisateurs de Ma Dada.

Si vous appréciez notre travail, vous pouvez contribuer en faisant un don sur [Liberapay](https://liberapay.com/madada) ou via [Helloasso](https://www.helloasso.com/associations/open-knowledge-france).
Vous pouvez également donner de votre temps en contribuant autrement : traductions, support, communication... toute aide est la bienvenue ! N'hésitez pas à nous écrire à [contact@madada.fr](mailto:contact@madada.fr) où à rejoindre notre communauté sur notre [forum](https://forum.madada.fr).
