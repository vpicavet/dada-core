# Gérer les demandes

Une demande est créée automatiquement dès qu'un utilisateur confirme son envoi. Ma Dada envoit la demande à l'autorité publique sélectionnée et va ensuite permettre de gérer la réponse ou l'abscence de réponse.

## Quel est le statut de la demande ?

Chaque demande a un statut qui indique sa progression. Normalement, une nouvelle demande aura comme statut "attente de réponse" jusqu'à ce qu'un évènement ne vienne le modifier, comme par exemple l'arrivée d'une réponse ou un retard de réponse.

Cependant, les statuts ne peuvent pas toujours être mis à jour automatiquement, car une décision doit petre prise sur le type de réponse apporté par l'autorité publique. Pour les statuts comme celui-ci, Ma Dada suggère aux personnes utilisatrices de décrire le nouvel statut de leur requête — par exemple, lorsqu'une réponse est apportée, les personnes utilisatrices peuvent choisir parmi les status abouti, partiellement abouti ou ne détiennent pas l'information demandée (si l'autorité publique a répondu qu'ils ne détiennent pas l'information demandée).

Pour changer ce paramètre, rendez-vous sur l'interface d'administration, cliquez sur le menu Requests puis cliquez sur le titre de la requête que vous souhaitez modifier. Puis cliquez sur le bouton "Edit metadata".

!!! note
Si une requête est restée plus de 3 semaines sans que l'évènement de réponse n'a entrainé de la part de la personne demandeuse une modification de son statut, alors n'importe qui peut modifier ce statut.

En interne, Ma Dada n'enregistre pas seulement le statut décrit de la requête, mais remarque aussi si quelque chose s'est passé depuis la dernière mise à jour de la requête et définit son statut à "en attente de description" conformément.

## Changer des choses sur une requête

Pour changer n'importe lequel de ces paramètres, rendez vous dans l'interface d'administration et cliquez sur Requests, puis cliquez sur le titre de la requête que vou souhaitez modifier. Cliquez sur le bouton "Edit metadata".

| Qu'est ce que vous pouvez changer ?          | Titre                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| -------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Titre                                        | Le titre apparaît sur la page de la requête mais apparait également dans l'url de la requête (le texte est mis en miniuscule, la ponctuation est supprimée et éventuellement un numéro est ajouté - pour éviter l'ambiguité). C'est le "slug" de la requête. Notez bien que changer le titre de la requête change égament son url, car le slug est modifié — ce qui signifie que tous les liens pointant vers l'ancienne url ne fonctionneront plus, résultant dans l'affichage d'une page d'erreur 404 (fichier non trouvé).                                                                                                                                |
| Qui peut la voir ?                           | Change le paramètre d'importance de la requête en utilisant l'un de ces statuts: <br>**- normal** <br>**-backpage** : la requête peut être vue par quiconque (en se rendant à son adresse url par exemple) mais n'appraît ni dans les listes ni dans les requêtes. Cette action définit également **X-Robots-Tag** avec une valeur d'entête "noindex" header pour empêcher que la page de la requête ne soit enregistrée par les moteurs de recherche.<br>**- personne demandeuse seulement** : la requête est uniquement visible par la personne qui l'a envoyée. <br>**- cachée** : la requête n'est pas visible (sauf par les personnes administratrices) |
| Qui peut répondre ?                          | Le paramètre "autoriser les réponses de la part de..." peut prendre les valeurs suivantes : <br>-**anybody** <br>- **authority_only** : les réponses sont autorisées uniquement si elles proviennent du nom de domaine associée à l'autorité destinataire, ou depuis n'importe quel domaine depuis lequel une réponse à cette requête a déjà été acceptée. <br>**- nobody**: aucune réponse n'est autorisée pour cette requête                                                                                                                                                                                                                               |
| Qu'est-ce qui arrive aux requêtes rejetées ? | Le paramètre de gestion des réponses rejetées définit ce qu'il arrive aux réponses qui ne sont pas autorisées : <br>**- bounce** : les réponses sont renvoyées à l'expéditeur <br>**- holding pen** : les réponses sont stockées dans l'espace tampon pour examen de la part des personnes administratrices <br> **- blackhole** : les réponses sont détruites en étant placées dans le trou noir                                                                                                                                                                                                                                                            |
| Dans quel état est-elle ?                    | Vous pouvez forcer le statut d'une requête en le choisissant manuellement. Il vous suffit pour cela de modifier le paramètre "Described state" dans le formulaire d'édition des métadonnées de la requête.<br>Il peut également s'avérer nécessaire de mettre le statut d'une requête sur la valeur "Awaiting description" si après en avoir modifié le statut vous souhaitez que la personne utilisatrice mette à jour la description du statut de la requête.                                                                                                                                                                                              |
| Est-ce que les commentaires sont autorisés ? | le paramètre "Are comments allowed" vous permet de décider si vous autorisez ou pas l'ajout d'annotations ou de commentaires sur une requête.<br>Notez cependant que la modificiation de ce paramètre n'entraîne pas l'occultation des commentaires qui auraient déjà été ajoutés.                                                                                                                                                                                                                                                                                                                                                                           |
| Tags                                         | Ajoutez des mots-clés, séparés par des espaces, qui sont associés avec une requête. Un tag peut être constitué d'un mot simple ou une paire clé-valeur (utilisez : comme séparateur, de cette manière clé:valeur).<br>Les mots clés sont utilisés pour la recherche. Les personnes utilisatrices ou administratrices peuvent en bénéficier car cela facilite l'identification de requêtes spécifiques notamment au fur et à mesure de l'augmentation du nombre de demandes effectuées. Bien que cela soit un peu plus complexe que les mots clés sur les requêtes, les catéories s'appuient également sur le mécanisme des tags.                             |

## Transférer une réponse envoyée par erreur dans la mauvaise demande

Lorsqu'une utilisatrice ou un utilisateur demande plusieurs documents au sein de plusieurs demandes à la même autorité publique, il peut arriver que l'autorité publique écrive sa réponse dans la mauvaise requête.

![copie d'écran du bouton de signalement](../media/action-signaler-demande.png)

Dans le menu d'administration il est possible de transférer la réponse vers la demande initiale correspondante. Pour réaliser cette action il faut récupérer l'identifiant de la réponse à déplacer et le saisir dans le champ prévu à cet effet dans le menu d'administration de cette requête.

Il faut sélectionner la réponse associée de manière incorrecte à une demande d'accès (url de type incoming_messages/[id-request]]/edit) en cliquant sur le lien "admin" associé à cette réponse.

Ensuite il faut trouver l'url de la demande vers laquelle doit être re-routée la réponse. Ici on peut utiliser l'url de la requête ou son indentifiant.

Si l'on souhaite rediriger une ou plusieurs réponses associées à la demande [demande/url-de-la-demande], l'url de la réponse sera du type [admin/incoming_messages/[id-requete]/edit]

En cliquant sur le lien {admin} situé en bas à droite de la réponse, un champs s'affiche intitulé "Redeliver message to one or more other requests". Vous pouvez saisir l'url ou l'identifiant vers lequel transférer la réponse acheminée de manière incorrecte.

A l'issue de la soumission de cette modification l'administrateur est redirigé vers la page de la requête vers laquelle a été redirigée la réponse.

A l'issue de ce transfert il est nécessaire de solliciter l'autrice ou l'auteur de la demande afin qu'il puisse mettre à jour le statut de sa requête initiale.

## Mettre à jour une demande nécessitant l'intervention de l'administration

En cas de problème d'acheminement d'une demande auprès d'une administration, l'utilisatrice ou l'utilisateur peut choisir dans le menu "Actions" de signaler la demande auprès des administratrices ou des admiinstrateurs du site.

![copie d'écran du bouton de signalement](../media/action-signaler-demande.png)

Un formulaire est proposé permettant de préciser la raison de ce signalement. Parmi les raisons disponibles on trouve :

- contient des propos diffamatoires
- ce n'est pas une demande valide
- demandes d'informations personnelles
- contient des informations personnelles
- offensante/inappropriée
- autre

A la suite de la soummission de ce formulaire, un email est envoyé sur la boîte fonctionnelle contact@madada.fr contenant le texte "requires_admin" ou "attention requested" avec un lien vers la demande concernée.

Cette information est également ajoutée au menu d'administration récapitulant l'ensemble des actions à réaliser de la part de l'administration du site (summary).

L'administration du statut de la demande est accessible via le lien "admin" situé à la suite du texte "demande de droit d'accès à l'information" dans la page publique de la demande ou directement depuis la rubrique "summary" en cliquant sur le lien de la requête concernée.

Une fois le problème réglé, il est nécessaire de mettre à jour le statut de la demande. Cette action peut être réalisée en cliquant sur le bouton "Edit Metadata" disponible sur la page d'administration de la requête.

Le champ à mettre à jour est intitulé "Described state". Il contient la liste des statuts disponible pour une requête :

- waiting_response : en attente de réponse
- waiting_clarification : demande de précisions
- gone_postal: réponse par courrier postal
- not_held : ne détient pas l'information demandée
- rejected : demande refusée
- successful : demande aboutie
- partially_successful : demande partiellement aboutie
- internal_review : demande de recours gracieux
- error_message : message d'erreur
- requires_admin : requière une action de l'administration
- user_withdrawn : demande retirée par l'utilisateur
- attention_requested : attention requise de la part de l'administration
- vexatious : demande offensante
- not_foi : pas une demande d'informations communicables

Après avoir changé le statut de la demande il faut mettre le champ "awaiting description" sur la valeur "yes - needs state updating" afin de permettre à l'utilisatrice ou l'utilisateur de mettre lui-même à jour le statut de la demande avec le bon statut. Dans le cas contraire c'est le statut défini par l'administration qui sera affiché.

Suite à cette intervention un email doit être envoyé à l'utilisatrice ou l'utilisateur responsable de la demande d'accès à l'information :

Bonjour,

Suite à votre signalement concernant la demande [Titre de la demande] nous sommes intervenus afin de corriger le problème signalé.

Vous pouvez vous rendre sur la page de la demande [lien vers la page de la demande] afin de mettre à jour son statut.

Merci pour ce signalement,

Cordialement,

L'équipe de Ma Dada

## Cacher une requête

Vous pouvez cacher entièrement une requête. Vous pouvez avoir besoin de réaliser cette opération notamment lorsque la requête ne concerne pas un accès à de l'information publique (mais une demande concernant des informations personnelles) ou si les termes de la demande sont injurieux.

Rendez-vous dans l'interface d'administration et cliquez sur Requests, puis cliquez sur le titre de la requête que vous voulez cacher. Vous pouvez la dissimuler de 2 manières:

- cacher la requête et notifier le demandeur
- cacher la requête sans notifier la personne utilisatrice

Les réponses à une requête cachée seront acceptées normalement mais comme elles sont attachées à la page de cette requête elle seront également cachées

Pour notifier la personne demandeuse, rendez-vous sur la section Actions de la page d'administration de la requête. Choisissez une des options disposnibles à côté de l'option "Hide the request" et notifiez la personne demandeuse:

- pas une requête d'accès à l'information publique valide "Not a valid FOI request"
- une reqête vexante "A vexatious request"

En choisissant une de ces 2 possibilités vous ferez apparaître un formulaire d'envoi de courriel. Modifiez le texte de l'email qui sera envoyé à la personne demandeuse, lui faisant connaître les raisons de votre action. Lorsque vous êtes prêt, cliquez sur le bouton "Hide request button".

Si vous ne souhaitez pas notifier la personne demandeuse, vous pouvez rendre la requête uniquement visible pour elle

Dans la section des métadonnées de la requête de la page d'administration de la requête, cliquez sur le bouton de modification des métadonnées. Modifiez la valeur d'importance en sélectionnant une de ces valeurs:

- requester_only: seul la personne demandeuse peut voir la requête
- cachée: personne ne peut voir la requête à part les personnes administratrices.

Si vous voulez cacher la requête, ne choisissez pas l'option "backpage" comme niveau d'importance. L'option "backpage" bloque l'apparaition de la requête dans les listes et les recherches mais les personnes qui connaissent son url peuvent toujours y accéder - la requête n'est pas cachée.
Lorsque vous êtes prêt.e enregistrez vos modifications. Aucun email ne sera envoyé à la personne demandeuse pour la notifier des modifications que vous avez réalisées.

# Censurer des informations à la demande d'un usager

- règles de censure (globales/locales)

## Censurer une pièce jointe en particulier

Si une PJ contient des informations sensibles (typiquement des données à caractère personnel):

- depuis la page de la demande, cliquer sur `Admin` pour le message contenant la PJ,
- dans la section "incoming messages", cliquer sur le titre du message à modifier,
- à la fin de la page, sous la section "Attachments", cliquer sur le titre de la PJ à cacher,
- puis sous "prominence", changer en `hidden`, en ajoutant une brève explication, et enregistrer.

A ce stade, la PJ en question n'est plus visible du public.

Au besoin, modifier le document, puis le sauvegarder sur
[madada.frama.space](https://madada.frama.space/index.php/apps/files?dir=/pj_demandes) dans le dossier `pj_demandes`.
Partager le document, puis ajouter un commentaire sur la page de la demande poitant vers ce lien.
