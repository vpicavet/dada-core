# Créer, modifier et gérer les personnes utilisatrices

Chaque personne souhaitant utiliser le site doit au préalable se créer un compte. Il est uniquement demandé de fournir un nom et une adresse email et de choisir son mot de passe.

L'intervention des personnes administratices du site est uniquement nécessaire pour accorder à la personne utilisatrice des droits étendus permettant d'accéder aux fonctionnalités de Ma Dada++.

L'intervention des personnes administratices peut également être nécessaire en cas d'identification de comportements inappropriés ou pour accorder des fonctionnalités spéciales.

## Bannir une personne utilisatrice

Il peut s'avérer nécessaire de bannir une personne utilisatrice s'il s'avère que ses activités sur le site portent préjudice à la ligne éditoriale ou autres personnes utilisatrices (une spammeur ou un harceleur par exemple). Cette action vise à empêcher la personne utilisatrice de continuer à utiliser les fonctionnalités du site. Pour cela vous devez vous rendre dans l'interface d'administration et sélectionner le menu "Users".

Trouvez la personne utilisatrice que vous souhaitez bannir dans la liste et cliquez sur leur nom. Sur la page affichant les détails de la personne utilisatrice, cliquez sur "Edit".

Entrez une description dans le champ ‘Ban text’ afin d'expliquer pourquoi vous désirez bannir cette personne. Attention cette information sera publiquement visible sur la page de la personne utilisatrice. A l'issue de la sauvegarde de ces informations la personne utilisatrice sera bannie.

## Supprimer le compte d'une personne utilisatrice

Il n'existe pas de moyen pour supprimer complètement une personne utilisatrice de Ma Dada, mais si vous avez vraiment besoin de désactiver le compte d'une personne utilisatrice vous pouvez suivre la procédure suivante:

Dans l'interface d'administration, trouvez la personne utilisatrice que vous souhaitez désactiver et cliquez sur "Edit"

Modifiez le nom de la personne utilisatrice en [compte désactivé] et supprimez les éléments de description

S'il y a une photo de profil, cliquez sur "clear photo" et supprimez la

Trouvez l'url du compte de la personne utilisatrice et modifiez son adresse email afin qu'elle
corresponde à cette url en ajoutant comme nom de domaine “invalid” comme ça: url_name@invalid

Lorsque vous avez enregistré ces modifications, identifiez vous en tant que cette personne utilisatrice en utilisant le bouton "Log in as…"

Cliquez sur "change your password", et confirmez en cliquant sur "Send me the email" — le système va essayer d'envoyer un email à l'adresse que vous avez modifié mais qui n'ira nulle part car cette adresse est invalide.

Toutefois vous pouvez accéder à ce lien de regénération de mot de passe en vous rendant sur la page d'administration de cette personne utilisatrice et en cliquant sur le lien de redirection dans la section "most recent Post".

Modifiez le mot de passe de la personne utilisatrice en choisissant une chaîne de caractères aléatoire

Si le compte a été au préalable banni, modifiez le texte expliquant les raisons du bannissement en choisissant des termes neutres.

## Anonymiser une personne utilisatrice

Parfois il est nécessaire de supprimer les détails du compte d'une personne utilisatrice de Ma Dada afin qu'elle ne puisse pas être identifiée par les moteurs de recherche. Pour faire cela, vous devez au préalable suivre la procédure ci-dessus, en remplaçant son nom par [name removed]. Vous devez ensuite supprimer toutes les références à cette personne utilisatrice sur le site. S'il y a beaucoup de requêtes sur le site cela peut constituer un travail important.

Vous pouvez ajouter une règle de réécriture afin de supprimer toutes les occurences du nom de cette personne utilisatrice en le remplaçant par le texte [name removed]. Cela implique de lire toutes les requêtes. Recherchez les noms (et peut-être les abbréviations) dans les numéros de références ou d'autres endroits où le nom pourrait apparaître. Il peut être nécessaire de ré-ordonner les règles de remplacement afin de s'assurer que toutes les formes du nom sont bien prises en compte. Vous pouvez en savoir plus en consultant la [section sur la ré-écriture](introduction.md#admin-censor-rule) dans la documentation.

## Permettre à une personne utilisatrice de faire plus de requêtes

Ma Dada dispose d'un paramètre de configuration `MAX_REQUESTS_PER_USER_PER_DAY`, qui détermine le nombre de requêtes qu'une personne utilisatrice peut réaliser par jour. Si elles essayent d'en faire plus que ce nombre sur une période de 24h, elles verront un message les informant qu'elles ont atteint la limite autorisée et les encourageant à utiliser le formulaire de contact si elles pensent avoir une raison justifiant l'augmentation de ce nombre maximum.

Pour augmenter la limite pour une personne utilisatrice en particulier, rendez vous sur l'interface d'administration, cliqez sur Users, puis sélectionnez le nom de la personne utilisatrice pour laquelle vous souhaitez modifier les paramètres. Cochez l'option "No rate limit", et enregistrez.

### Gestion des rôles

En tant qu'administratrice ou administrateur du site il est possible de donner des droits supplémentaires (role) aux personnes utilisatrices (en utilisant le menu User qui s'affiche sous la forme d'un bandeau noir lorsque l'on est connecté en tant que personne administratrice) :

Actuellement les trois rôles suivants existent sur la plateforme :

- `admin`: confère des droits d'administration sur le site,
- `pro`: permet d'accéder aux fonctionnalités avancées de la version Ma Dada++,
- `pro_admin` : permet d'administrer les besoins des personnes dôtées du rôle `pro` comme les demande en masse ou les demandes sous embargo.

![gestion des rôles](../media/admin-create-admin.png)
