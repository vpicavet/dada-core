# Guide d'administration de Ma Dada

**De quoi s'agît-il ?**

Pour bien fonctionner au quotidien, la plateforme Ma Dada fait appel à une équipe de personnes (volontaires pour la plupart) qui assure le support aux utilisatrices et utilisateurs, ainsi que des opérations de maintenance usuelle. Cette documentation a pour but d'aider et de guider cette équipe dans ce travail.

!!! note

    Pour des raisons de praticité on désignera dans la suite de ce guide par le terme « admins » les personnes ayant des droits d'administration à différents niveaux sur Ma Dada.

## Support aux personnes utilisatrices du site

Parmi les tâches nécessitant l'intervention des admins Ma Dada on peut lister les cas d'usages les plus courants suivants :

- l'email envoyé ne parvient pas à l'autorité publique concernée
- demande de modification ou d'ajout d'une autorité publique
- traitement du signalement d'une demande sur la plateforme
- traitement du message "cette demande requiert l'attention d'un administrateur"
- question sur l'utilisation de la plateforme
- question sur le droit d'accès aux documents administratifs
- autre assistance d'ordre général requise
- problème d'affichage de messages ou de pièces jointes
- demande de création de compte Ma Dada ++
- etc.

La section [Guide de support aux admins de Ma Dada](guide-support-admins.md) détaille un grand nombre de cas et vous guide dans la gestion de chacun de ceux-ci. Ce sera ce guide qui vous indiquera en détail comment gérer en pratique les situations les plus courantes.

La section [Gestion des usagers](gestion-usagers.md) qui explique les différents types d'utilisateurices chez Ma Dada et la gestion administrative de leurs comptes et droits.

La section [Gestion des autorités](gestion-autorites.md) qui explique comment les autorités sur Ma Dada sont crées et mises à jour.

La section [Gestion des demandes](gestion-demandes.md) qui explique les différents statuts et étapes de vie des demandes sur Ma Dada.

## Outils de communication Ma Dada

Ma Dada utilise les outils de communication suivants :

- [Compte Twitter](https://twitter.com/madadafr/)
- [Compte Mastodon](https://mamot.fr/@madadafr), notre identifiant Mastodon étant `@madadafr@mamot.fr`
- Outil de cross-posting Twitter et Mastodon : https://moa.party/
