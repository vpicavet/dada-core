# Gestion des autorités publiques dans Ma Dada

## Demande d'ajout ou de modification d'une nouvelle autorité administrative

Il s'agît ici des demandes les plus fréquentes de la part des utilisateurices. Ces demandes arrivent par mail chez Ma Dada. Ce mail se présente comme suit :

![nouvelle autorité](../../media/mail-nouvelle-autorite.png)

Voici la procédure à suivre pour l'ajout de l'autorité :

1. Cliquer sur le lien dans le champs "Ajouter l'autorité publique". S'ouvre alors une page web avec le formulaire d'ajout d'autorité Ma Dada comme ci-dessous :

![formulaire Ma Dada d'ajout de nouvelle autorité](../../media/formulaire-ajout-autorite.png)

2. Dans le formulaire, commencer par vérifier le nom de l'autorité. Il s'agît ici de s'assurer :


    - que l'autorité à ajouter n'existe pas déjà dans Ma Dada sous le même nom ou un nom similaire. Pour vérifier ceci effectuer une recherche par mot clé [parmi les autorités sur le site de Ma Dada](https://madada.fr/body/list/all);

    - de la bonne orthographe du nom de l'autorité, en vérifiant celle-ci par exemple sur la page web officielle de l'autorité ou sur Wikipedia.

3. Remplacer le mail de l'autorité publique par, dans l'ordre de préférence :


    - le mail de la PRADA de l'autorité si jamais elle existe. Pour le vérifier télécharger et rechercher dans l'annuaire des PRADAs de la CADA sur le site de la CADA : https://www.cada.fr/lacada/annuaire-des-prada. Ne pas prendre un ancien annuaire téléchargé auparavent car il se peut qu'une nouvelle mise à jour ait eu lieu entre temps;

     - le mail renseigné par la personne qui demande l'ajout de l'autorité, après vérification. La plupart du temps la personne va chercher le mail sur le site web de l'autorité. Vérifier que le mail se trouve bien soit sur le site internet de l'autorité, soit sur le web et qu'il est bien renseigné sans fautes d'orthographe;

     - si aucun mail n'est renseigné par la personne, on peut scruter le site web de l'autorité pour en trouver. Scruter en particulier les pages contact, mentions légales, open data, RGPD et politique de cookies;

     - sinon scruter l'annuaire du service public des administrations : https://lannuaire.service-public.fr/;

     - ou essayer de chercher "@nomdedomainedel'autorité.fr" sur le web;

     - ou en dernier recours appeler le standard téléphonique de l'administration et demander un mail de contact.

4. Ajouter l'adresse postale de l'autorité. Dans le cas d'un Prada recopier l'adresse depuis l'annuaire Cada (attention à ajuster la mise en page). Sinon chercher l'adresse sur le site de l'autirité. Sinon dans l'annuaire du service public ci dessus.

5. Ajouter le lien vers le site web de l'administration (la version https plutot que http si elle existe).

6. Ajuster à la fin du formulaire le mail automatique qui sera envoyé à la personne qui a fait la demande d'ajout. Il comporte éventuellement des erreurs (en juin 2022 il y a tout un paragraphe en début de mail à enlever, le paragraphe rappelant la loi). Si vous avez ajouté une autre adresse que celle suggéré par la personne, pensez à lui indiquer pourquoi. Par exemple comme ci-dessous :


    > Madame, Monsieur,
    > Merci de votre suggestion d'ajouter l'autorité X. Nous l'avons ajoutée ici : [L'URL de l'autorité publique sera insérée ici]. Cependant, nous n'avons pas repris l'adresse mail que vous aviez trouvé comme contact pour l'autorité, car nous avons remarqué que cette autorité est présente dans l'annuaire des PRADA de la CADA, nous avons donc repris l'adresse mail PRADA qui y était renseignée (https://www.cada.fr/lacada/annuaire-des-prada). N'hésitez pas à nous signaler des erreurs d'acheminement du mail si vous remarquez des anomalies dans vos demandes.
    > Sincèrement,
    > L'équipe de Ma Dada.

7. Toujours mettre un commentaire dans le champ dédié du formulaire d'ajout et y indiquer toutes les sources utilisées pour remplir le formulaire. Exemple : « ajout d'autorité selon annuaire PRADA en date du (copier-coller la date de mise à jour sur le site de l'annuaire PRADA de la Cada en respectant le format sur le site)».

8. Soumettre le formulaire d'ajout. Vous verrez que le mail édité plus haut est automatiquement envoyé à la personne ayant fait la demande d'ajout pour lui notifier l'ajout effectif.

9. Il sera souvent nécéssaire après modificication du mail de contact d'une autorité de vérifier la liste des mails récemment envoyés à cette autorité par les différents utilisateurs. Il y a des chances que des demandes récentes n'ait pas abouties, ou soit marquées en erreur, car envoyées à la mauvaise ancienne adresse. Il faut dans ce cas les renvoyer vers la nouvelle adresse de l'autorité. Pour cela, à partir de la page pincipale de l'autorité dans l'interface admin de Ma Dada, dans la sections « Requests » qui se présente comme ci-dessous:
   ![interface Ma Dada admin d'une autorité](../../media/madada-screenchot-list-requests.png)

et qui liste les demandes en cours pour cette autorité. cliquez sur chacune de ces demandes pour voir s'il faut la renvoyer. On se retrouve alors dans l'interface d'administration de la demande. Pour renvoyer la demande, aller dans la section "Outgoing messages" de cette interface, puis cliquer sur le lien en bleu « Sent request ».
![interface Ma Dada admin de messages envoyés](../../media/madada-screenshot-outgoing-messages.png)
On se retrouve alors dans l'interface d'envoi des mails. Cliquez sur le bouton Resend tout en bas du formulaire (sans changer le contenu du mail) :
![formulaire Ma Dada admin d'édition des mails envoyés](../../media/madada-screenshot-send-request-again.png)

Ceci renvoie la demande à nouveau, mais cette fois à la nouvelle adresse modifiée plus haut de l'autorité. On voit alors dans l'interface publique de la demande, apparaitre le message suivant :
![interface Ma Dada publique en cas de renvoi de demande](../../media/madada-screenshot-resent.png)

## Créer un fichier des autorités publiques existantes

Vous pouvez facilement créer un tableur numérique contenant les autorités publiques déjà existantes sur Ma Dada. Combiné à la fonctionnalité d'import en masse décrite ci-dessus, cela peut constituer le moyen le plus efficace de mettre à jour les plutot que de les changer manuellement via l'interface d'administration.

Pour exporter les données des autorités publiques existantes, allez sur la page d'accueil de Ma Dada et cliquez sur "Voir les autorités publiques". Puis cliquez sur le lien [Liste de toutes les autorités](https://madada.fr/body/all-authorities.csv) afin de récupérer le fichier csv. Vous pouvez mettre à jour ce fichier et l'importer comme décrit ci-dessus.

Vous devrez supprimer certaines colonnes qui ne sont pas acceptées par la procédure d'importation et éventuellement renommer certaines qui le sont. Notez également que par défaut le fichier exporté ne contient pas la colonne request_email. Si vous souhaitez mettre à jour des adresses email, vous devez manuellement ajouter cette colonne et remplir le champs avec l'adresse email que vous souhaitez mettre à jour.
