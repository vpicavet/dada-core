# Charte d'usage

## Préambule

En créant un compte sur madada.fr, vous vous engagez à adhérer à la présente charte.

MaDada est une plateforme opérée par l'association Open Knowledge France visant à faciliter la demande de documents administratifs.

Les demandes de documents administratifs n'ont pas nécessité à être effectuées par une personne nommément identifiée. Toutefois, en cas de recours auprès de la CADA ou de saisine du tribunal administratif, il pourra être exigé de votre part de fournir un justificatif d'identité. Pendant la phase initiale, la plateforme transmet votre demande par le biais d'une adresse email dédiée. Ce mécanisme permet de rendre les échanges publics et de dissimuler votre adresse email. Cependant, à moins que vous ayiez un motif légitime, nous vous encourageons à signer votre courrier avec votre vraie identité car vous êtes légitime à exercer votre droit d'accès à l'information publique. Si vous utilisez un pseudonyme ou que vous usurpez une identité publique, l'équipe de modération se réserve le droit de masquer votre demande afin d'éviter les risques de procédures en diffamation. De la même manière si votre demande concerne des informations à caractère privée, elle sera supprimée de la plateforme par l'équipe de modération.

## Conditions d’accès au service MaDada++

MaDada dispose d'une version augmentée nommée Ma Dada++. Elle est a priori accessible à toutes et tous sur simple demande à l'aide de ce [formulaire](https://madada.fr/pro).

Nous vous demandons uniquement de créer au préalable un compte et d'effectuer une première demande avant de demander l'activation des fonctionnalités avancées.

Afin d'éviter que la plateforme ne serve à spammer des autorités publiques, nous vous demandons simplement de justifier votre besoin d'utilisation de ces fonctionnalités avancées. Vous êtes journaliste d’investigation ou vous souhaitez effectuer des recherches sur le bilan carbone d'autorités publiques. Ces fonctionnalités sont pour vous!

En effet, une fois votre compte MaDada++ activé vous pourrez envoyer la même demande à de nombreuses autorités administratives simultanément. Afin de conserver la crédibilité de notre serveur de mail nous sommes donc vigilants à ce que celle-ci ne soit pas utilisée pour d'autre objet que les demandes d'accès à l'information.

En complément, vous pouvez choisir de masquer la visibilité de votre demande d'accès à des documents administratifs en plaçant celle-ci sous embargo pour la durée de votre choix (3, 6 ou 12 mois). Cette fonctionnalité est destinée aux journalistes, activistes ou chercheuses ou chercheurs pendant la durée de leur travail d'investigation. Toutefois, à l'issue de cette période, vous vous engagez à rendre public vos demandes car notre but est de contribuer à l'accès ouvert à l'information.

<a name="utilisateurs"></a>
## Les utilisateurs

En utilisant le service en ligne MaDada vous vous engagez à ne pas l'utiliser pour envoyer des courriels non-désirés aux autorités publiques. Vous vous engagez également à ne pas diffamer ou insulter les autorités administratives ou les représentantes et représentants.

Vous vous engagez également à utiliser un ton courtois lors de vos échanges avec les autorités publiques saisies.

En utilisant ce service vous vous engagez à ne pas faire de demande à caractère privé visant à l'obtention de documents contenant des données privées et/ou nominatives.

Si vous rencontrez un problème dans l'utilisation du service ou que vous avez une question, n'hésitez pas à contactez l’équipe de MaDada en utilisant ce [formulaire](https://madada.fr/aide/contact).

## Les autorités publiques

Lorsque vous répondez à une demande d'accès à un document administratif, vos réponses sont publiques. Veuillez vérifier le contenu de vos email (signature, etc.) afin d'éviter que des données nominatives ou personnelles dont vous ne souhaiteriez pas la diffusion publique y soient mentionnées.

Lorsque vous recevez une demande d'accès à un document administratif, merci de répondre à l’adresse email fournie par notre service.

Si vous rencontrez un problème dans l'utilisation du service ou que vous avez une question, n'hésitez pas à contactez l’équipe de MaDada en utilisant ce [formulaire](https://madada.fr/aide/contact).

## L’équipe MaDada

Nous adoptons une position neutre vis-à-vis des documents demandés aux autorités publiques et ne soutenons aucune cause partisane ou politique. Le but de la plateforme MaDada est de faciliter l'exercice du droit d'accès à l'information, tel qu'établi par la loi. Nous oeuvrons pour que chaque citoyenne ou citoyen puisse exercer ce droit fondamental. Pour ce faire nous entretenons des relations avec les autorités publiques ou la Commission d'accès aux documents administratifs (CADA) afin de contribuer à la simplification des démarches associées.

En conséquence, l'équipe de modération de MaDada se réserve la possibilité de masquer ou de supprimer des demandes contenant des propos injurieux ou diffamants.

L'équipe d'administration et de modération de MaDada est au service des utilisatrices et des utilisateurs et des administrations afin de faciliter l'exercice du droit d'accès à l'information.

Nous efectuons régulièrement des mises à jour de l’annuaire des autorités publiques. Si vous constatez une erreur ou que vous souhaitez l'ajout d'une nouvelle autorité publique, merci d'utilisez ce [formulaire](https://madada.fr/aide/contact).

MaDada est une plateforme associative. Bien que nous fassions notre possible pour faciliter votre droit d’accès à l’information, nous ne pouvons en aucun cas garantir le résultat de vos demandes, ni notre capacité à intervenir dans des délais précis.
