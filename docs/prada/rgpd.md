## Demander le retrait d'informations à caractère personnel

Si vous souhaitez demander le retrait de données à caractère personnel de Ma Dada, [ce conseil de la
CADA](https://www.cada.fr/20213227) de Juillet 2021 permettra de
faciliter la procédure.

En particulier, les coordonnées professionnelles des PRADA sont des informations publiques.

Concernant les autres membres de l'administration, la CADA recommande que leurs informations à caractère professionnel
soient supprimées des réponses avant envoi à Ma Dada.
