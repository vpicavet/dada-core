## Ma Dada, c'est quoi?

Ma Dada est une plateforme associative et citoyenne visant à faciliter l'exercice du droit d'accès à l'information. Cette plateforme s'appuie sur le Code des relations entre le public et l'administration (CRPA), qui définit les modalités d'accès aux documents administratifs et de réutilisation des informations publiques que ces documents contiennent.

Nous proposons aux internautes un annuaire des autorités publiques leur facilitant l'identification de l'administration à laquelle adresser leur demande. Cet annuaire a été construit à partir des informations contenues dans le site service-public.fr, et dans l'annuaire des Personnes responsables de l'accès aux documents administratifs (PRADA) maintenu par la Commission d'accès aux documents administratifs (CADA).

Dans le livre III du CRPA, les articles L330-1 à L330-4 précisent l'obligation faite aux autorités administratives de nommer en leur sein une personne responsable de l'accès aux documents administratifs.

L'article L330-2 précise les typologies d'autorités administratives concernées :

- Les ministères
- Les préfectures
- Les régions
- Les départements
- La collectivité territoriale de Corse
- Les communes de 10 000 habitants ou plus
- Les établissements publics de coopération intercommunale de 10 000 habitants ou plus
- Les établissements publics nationaux et locaux qui emploient au moins deux cents agents
- Les autres personnes de droit public et les personnes de droit privé chargées de la gestion d'un service public qui emploient au moins deux cents agents

Les autorités administratives concernées doivent porter à connaissance du public les nom, prénoms, et coordonnées professionnelles de leur PRADA - notamment en les diffusant sur leur site Internet (si elles en ont un).

Les coordonnées des PRADA sont donc des informations publiques et la légalité de leur publication sur notre site a été confirmée dans ce [conseil de la CADA](https://www.cada.fr/20213227). Si la PRADA concernée préfère que son identité ne soit pas rendue publique sur le site de la CADA et sur le nôtre, il lui appartient de créer une adresse email fonctionnelle non nominative.

Afin de pouvoir suivre les réponses, nous utilisons des adresses électroniques générées par ordinateur pour chaque demande. En tant qu’administration, vous recevrez donc des demandes provenant d'une adresse telle que `dada+request-123456-abc123@madada.fr`. En répondant directement à cette adresse, votre réponse sera automatiquement aiguillée vers la demande correspondante sur notre site.

## Autorités publiques : que faire en cas de réception d'un email ?

La demande d'accès que vous avez reçue a été faite par un utilisateur de Ma Dada. Vous pouvez simplement répondre à la demande via l’email reçu.

Nous attirons cependant votre attention sur le fait que **votre réponse sera automatiquement publiée sur Ma Dada**. Il vous appartient donc d'occulter les informations personnelles contenues dans les documents, et de répondre à la demande reçue de préférence avec une adresse email fonctionnelle (non nominative) sans mention de vos coordonnées personnelles (email, numéro de téléphone professionnel, adresse professionnelle) si vous ne souhaitez pas que de telles informations soient publiées sur notre site.

Pour empêcher le spam, nous retirons automatiquement les adresses emails et numéros de téléphone des réponses aux demandes. N'hésitez pas à [nous contacter](https://madada.fr/aide/contact) en cas d'oubli. Pour des raisons techniques, nous ne retirons pas toujours ces informations des fichiers joints, comme certains PDF. Si vous souhaitez connaître l'adresse email retirée, contactez-nous.

Nous sommes toujours ravis de recevoir des commentaires de la part des PRADA, n'hésitez donc pas à nous contacter.

## Je peux voir une demande sur Ma Dada, mais je n'ai jamais reçu d'email !

Si une demande apparaît sur le site, cela signifie que nous avons tenté de l'envoyer par email. Tous les messages d'échec de livraison apparaîtront automatiquement sur le site. Vous pouvez vérifier l'adresse que nous utilisons en cliquant sur le lien "Voir l'adresse email de demande d'accès à l'information" qui apparaît sur la page de l'autorité publique. Contactez-nous s'il y a une meilleure adresse que nous pouvons utiliser.

Les demandes n'arrivent parfois pas parce qu'elles sont bloquées par les “filtres anti-spam” de votre serveur mail. Les autorités peuvent s’assurer que cela ne se reproduise plus en demandant à leur service informatique de rajouter les emails @madada.fr à la liste des emails autorisés. A votre demande, nous pouvons vous renvoyer les demandes déjà réalisées et/ou vous donner des détails techniques concernant le problème de livraison afin que votre service informatique puisse régler le problème.

Enfin, vous pouvez répondre à toute demande depuis votre navigateur, sans avoir besoin d'email, en utilisant simplement le lien "répondre à la demande", en bas de chaque page de demande. Pour cela, il vous faudra au préalable créer un compte sur notre site avec comme adresse email de contact une adresse email professionnelle.

## Gestion des délais

Les autorités publiques sont tenues de répondre sous un mois à compter de la date de réception de la demande de communication de documents administratifs. Ma Dada indique la durée légale maximale de réponse pour chaque demande.

Le jour où la demande est délivrée par email est compté comme le "jour 0", même si la demande a été délivrée tard dans la soirée. Les journées finissent à minuit. Nous comptons ensuite le jour suivant comme le "jour 1", et ainsi de suite jusqu'à 30 jours.

En cas de décision de refus d'accès aux documents administratifs demandés, vous devez notifier au demandeur votre refus sous la forme d'une décision écrite motivée comportant l'indication des voies et délais de recours. Le demandeur dispose d'un délai de deux mois à compter du refus d'accès aux documents administratifs qui lui est opposé pour saisir la Commission d'accès aux documents administratifs.

Si vous avez connaissance d'un motif pouvant justifier un délai de réponse important, il est recommandé d'en informer brièvement les demandeurs par email à ce sujet. Les PRADA ont souvent beaucoup de travail à fournir pour répondre à certaines requêtes, mais ce processus reste invisible et inconnu du public. Nous pensons que cela aiderait tout le monde de rendre cette complexité de traitement plus visible.

## Comment puis-je envoyer de gros fichiers qui ne passent pas par email ?

Au lieu d'utiliser l'email, vous pouvez répondre à une requête directement depuis votre navigateur et transmettre ainsi des pièces jointes volumineuses. Pour faire cela, choisissez "Répondre à une demande" en bas de la page de la demande. Contactez-nous si le fichier est trop gros, même pour notre outil (au-delà de 50Mb).

## Demander le retrait d'informations à caractère personnel

Avant de demander le retrait de données à caractère personnel, nous vous invitons à parcourir ce [conseil de la CADA de juillet 2021](https://www.cada.fr/20213227-0).

En particulier, les coordonnées professionnelles des PRADA sont des informations publiques.
Concernant les autres membres de l'administration, la CADA recommande que leurs informations à caractère professionnel soient supprimées des réponses avant envoi à Ma Dada.
Pour toute demande en lien avec des données personnelles, vous pouvez nous contacter à dpo@madada.fr.

## Droits d'auteur

Si vous pensez que notre mise à disposition sur Internet d'un document enfreint vos droits d'auteur, vous pouvez nous contacter pour solliciter un retrait.

## Suppression des réponses anciennes

Après six mois d'inactivité, Ma Dada limite qui peut répondre à une demande afin d'empêcher les spammeurs de s'en prendre aux anciennes demandes. A ce stade, la demande ne peut être traitée que par une personne travaillant pour l'administration concernée. Après un an, la demande est fermée, plus personne ne peut y répondre. Si vous souhaitez ré-ouvrir une ancienne demande pour y répondre, merci de nous contacter.
