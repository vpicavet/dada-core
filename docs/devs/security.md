# Quelques notes sur la sécurité

Bien que [Ma Dada](https://madada.fr) ait pour but d'ouvrir des données, il y'en a quelques unes que nous tenons à
garder privée.

`Données personnelles des usagers`

:   Bien qu'on ne collecte que peu d'information (l'adresse email est la seule donnée qui n'est pas publiquement
visible), il n'est pas acceptable que celles-ci soient divulguées.

`Demandes sous embargo`

:   Durant la période d'embargo, une demande doit rester confidentiel. Seuls son auteur.e et les admins du site peuvent
y avoir accès. Sa seule existence doit rester confidentielle jusqu'à expiration de l'embargo. On ne s'autorise à parler
des demandes sous embargo que de manière générale: leur nombre cumulé est [public](https://madada.fr/stats) mais pas
l'identité des personnes qui les ont rédigées, ni à quelle autorités elles sont envoyées, ou leur contenu et réponses.

## Audit sécurité de 2022

Dans le cadre de notre financement par NLNet, un audit sécurité a été réalisé en Mai 2022. Les [résultats
détaillés](https://gitlab.com/madada-team/dada-core/-/issues/71) ne
sont pas rendus publics étant donné qu'ils peuvent affecter d'autres sites utilisant alaveteli.

## Impact sur le développement du site

Pour avoir accès au dépôt gitlab et pousser du code, il faut avoir activé la [double autentification](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html)(`2FA`) sur gitlab.
