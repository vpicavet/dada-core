# Traduction de Ma Dada

La version "principale" d'Alaveteli est développée en Anglais, ainsi que tous les textes du site.

Notre processus de déploiment utilise plusieurs systèmes pour traduire le logiciel en Français:

- [transifex](https://transifex.com): l'outil principal de traduction
- Certaines pages (majoritairement statiques) sont traduites dans le thème alaveteli.
- certaines chaines de caractères sont traduites à la main dans notre playbook ansible lorsque le code source parent ne
  supporte pas la localisation pour un texte en particulier.

## Trouver où traduire

La logique pour trouver où traduire un bout de texte en particulier est à peu près la suivante:
- Vérifier si la chaine figure dans `ansible/roles/alaveteli/tasks/main.yml`. Si c'est le cas, ajuster la tranduction
  dans ce fichier.
- Sinon, chercher dans le dossier `dada-france-theme` si la page où apparaît le texte y figure.
- Sinon, chercher dans transifex (voir plus bas).
- Dans certains cas, le texte en question n'est tout simplement pas traduisible sans modifier le code source
  d'alaveteli. Chercher "Add pagination translation" dans ce dépôt pour un exemple.

## Modifier une traduction dans transifex

Ma Dada utilise [transifex](https://transifex.com) pour suivre le reste de la communauté alaveteli.

Il y a plusieurs versions francophones disponibles:

- [`fr_BE`](https://www.transifex.com/mysociety/alaveteli/language/fr_BE/) est utilisée par le site Belge [transparencia](https://transparencia.be)
- [`fr`](https://www.transifex.com/mysociety/alaveteli/language/fr/) (tout court) est utilisée par le site européen [AskTheEU](https://asktheeu.org)
- [`fr_FR`](https://www.transifex.com/mysociety/alaveteli/language/fr_FR/) est la traduction utilisée sur notre site.
  Attention à bien travailler sur celle-ci!

Les traductions sont séparées en deux fichiers ressources:

- `Alaveteli Pro` qui inclut tout ce qui touche aux fonctions "Pro" (Ma Dada++),
- `app.pot` qui contient tout le reste.

Il est parfois un peu difficile de trouver où se cache un morceau de texte en particulier. Chercher dans les deux
fichiers est souvent une bonne idée, même s'il semble "évident" où le texte devrait se trouver.

### Demander l'accès aux traductions

Si vous n'avez jamais traduit alaveteli, il vous faudra dans l'ordre:

- créer un compte transifex [ici](https://www.transifex.com/signup/)
- demander l'accès aux traductions du site. Cliquez [ici](https://www.transifex.com/mysociety/alaveteli/language/fr_FR/)
  une fois connecté.e à votre compte transifex. Un message d'erreur devrait apparaître, il ne reste plus qu'à demander à
  faire partie de l'équipe de traduction française. L'équipe Ma Dada devrait recevoir une notification rapidement.
  N'hésitez pas à nous contacter par email en parallèle si nous ne vous connaissons pas pour nous expliquer qui vous
  êtes.

### Traduire le site

Une fois que vous avez accès aux traductions, vous devriez pouvoir:

- Voir les fichiers de traductions ("Resources" sur transifex) [ici](https://www.transifex.com/mysociety/alaveteli/language/fr_FR/)
- Choisir celui qui vous intéresse en cliquant dessus, puis sur le bouton bleu `translate` dans la fenêtre qui apparaît.
- L'interface de traduction proprement dite s'ouvre alors. Il y'a beaucoup de fonctionnalités, mais le cas d'usage le
  plus courant pour Ma Dada consistera à modifier une traduction préexistante. Pour ce faire, utiliser la fonction de
  recherche en haut à gauche de l'écran `Search by text or filter`. Deux solutions ici (Attention aux guillemets simples dans les deux cas!
):

    - Rechercher par texte en français (tel qu'il apparaît sur le site) avec: `translation_text:'le texte que je cherche'`
    - Rechercher par texte en anglais (l'original, tel qu'il apparaît dans le code source d'alaveteli) avec `text:'the
      original text'`.

- Une fois la bonne chaîne de caractère repérée, traduire dans la section centrale de la page, puis cliquer `Save
  changes`.

L'aide de transifex fournit des [instructions
détaillées](https://docs.transifex.com/translation/translating-with-the-web-editor) pour apprendre à utiliser leur
interface efficacement.

!!! tip "Attention aux traductions"

    Attention en particulier à ne pas traduire mot à mot: les réglementations brittanique (pour lesquelles le texte
    original est écrit) et française sont assez différentes, et il faut parfois adapter le texte au contexte de la page
    sur laquelle il apparaît.

    Nous avons créé une [page de terminologie](/devs/terminologie) pour Ma Dada qui recense les expressions les plus couramment utilisées avec
    leurs équivalents originaux si nécessaire.

### Déploiement des traductions

Le processus de déploiement de Ma Dada récupère les dernières traductions à chaque déploiement du site (soit vers le
site de staging, soit vers le site de production).
