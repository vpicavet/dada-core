# Upgrading the site (alaveteli and dependencies)

`mySociety` regularly [publishes updates](https://github.com/mysociety/alaveteli/releases) for alaveteli, which we
should follow as regularly as possible.

## Upgrading alaveteli

[Release notes](https://github.com/mysociety/alaveteli/blob/develop/doc/CHANGES.md) include an `Upgrade Notes` section for each release. Start by reading them carefully to figure out what needs to be done in the upgrade. Read the whole thing before starting to work. This typically includes maintenance upgrades of ubuntu, postgresql, ruby, etc...
This doc will not cover how to upgrade these as each version will have its specificities, and they are usually described
pretty well elsewhere.

!!!note "Upgrade often"
    Do keep in mind that such upgrades get harder as you wait to do them ;) So upgrade often if possible. The team at
    Alaveteli usually sends out a notification email to the [alaveteli-dev mailing list](https://groups.google.com/g/alaveteli-dev) a few days before they cut a release. Subscribe to the mailing list to get them, and plan an upgrade as soon as possible after the release comes out.

## Planning upgrades

Unless you're doing a very minor upgrade, which should be handled automatically during deployment, you will render the
site unavailable for some time, from just a few minutes to a few hours. It is best to plan such downtimes during weekends, when the
site is least used. Start by turning off nginx to prevent public access to the site with `sudo service nginx stop`
(remember that the CI deployment will bring it back up).

Make sure to run the upgrade on the staging server (and verify that the site is still working there) before you apply
the same changes to the production server.

## Upgrading the playbook

We keep the ansible playbook functional, so if a version upgrade requires new commands, dependencies or whatever, the
first place to look at and modify would be the playbook. Only do manual operations on servers if they are going to be
one-offs.

### Upgrading templates

As we use a customised theme, we need to adjust templates to match the alaveteli structure, otherwise pages will most
likely break or not be rendered at all. The upgrade notes lists all templates that were modified since the previous
release. You need to check this list against the templates we have modified, and adjust the ones that need it.
