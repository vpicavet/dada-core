stages:
  # la CI est séparée en plusieurs jobs indépendants:
  # test est utilisé pour la detection de secrets dans l'include à la fin,
  # et plusieurs linter (ansible, ruby...)
  - test
  # le site principal est déployé depuis les branches staging/master
  - main_site
  # la doc uniquement depuis doc-public
  - docs
  - schedule

.theme_tests:
  stage: test
  # use a readymade ruby image (debian based, while the server runs on ubuntu)
  # This is to get around ruby install problems that seem too annoying to bother solving
  image: ruby:3.2.2-bookworm
  services:
    - postgres:13
  variables:
    POSTGRES_DB: alaveteli_test
    POSTGRES_USER: postgres
    POSTGRES_PASSWORD: postgres
    POSTGRES_HOST_AUTH_METHOD: trust
    DATABASE_URL: postgres://postgres:postgres@localhost:5432/alaveteli_test
    RAILS_ENV: test

  before_script:
    # - mkdir -p /opt/hostedtoolcache/Ruby/3.2.2/
    - pwd
    - ls -l
    - mkdir core
    - apt-get -y update
    - apt-get -y install exim4-daemon-light git wget ruby-dev libmagic-dev
    - git clone https://github.com/mysociety/alaveteli --depth 1 --shallow-submodules --recurse-submodules core
    - cd core
    - apt-get -y install `cut -d " " -f 1 config/packages.ubuntu-focal | egrep -v "(^#|wkhtml|bundler|^ruby|^rake)"`
    - mkdir alaveteli-themes
    - cp -R ../dada-france-theme alaveteli-themes/
    - ls -l alaveteli-themes/
    # - wget -nv -O /opt/hostedtoolcache/ruby-3.2.2-ubuntu-22.04.tar.gz https://github.com/ruby/ruby-builder/releases/download/toolcache/ruby-3.2.2-ubuntu-22.04.tar.gz
    # - /usr/bin/tar -C /opt/hostedtoolcache/Ruby/3.2.2/ -xzf /opt/hostedtoolcache/ruby-3.2.2-ubuntu-22.04.tar.gz
    # - export PATH=/opt/hostedtoolcache/Ruby/3.2.2/x64/bin:$PATH
    # - export LD_LIBRARY_PATH=/opt/hostedtoolcache/Ruby/3.2.2/x64/lib:$LD_LIBRARY_PATH
    # - echo $PATH
    # - echo $LD_LIBRARY_PATH
    # - ls -l /opt/hostedtoolcache/Ruby/3.2.2/x64/bin/
    # - ls -l /opt/hostedtoolcache/Ruby/3.2.2/x64/lib/ruby/3.2.0/x86_64-linux/
    - ruby --version
    # - command -v bundle
    - bundle config set --local path 'vendor/bundle'
    - bundle install -V
    - |
      psql postgres://postgres:postgres@postgres:5432 <<-EOSQL
        CREATE DATABASE template_utf8 TEMPLATE template0 ENCODING "UTF-8";
        UPDATE pg_database SET datistemplate=true WHERE datname='template_utf8';
        CREATE DATABASE alaveteli_test TEMPLATE template_utf8;
      EOSQL
    - cp config/general.yml-example config/general-dada-france-theme.yml
    - cp config/storage.yml-example config/storage.yml
    - script/switch-theme.rb dada-france-theme
    - bundle exec rake themes:install
    - bundle exec rails db:migrate
  script:
    - bundle exec rspec --format Fivemat lib/themes/dada-france-theme/spec
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_BRANCH

deploy_madada:
  stage: main_site
  image: python:3.8
  before_script:
    # install ansible for provisioning of server (setup db, nginx, MTA...)
    - echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main" >> /etc/apt/sources.list
    - apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
    - apt-get update -y
    - apt-get install -y ansible openssh-client python3-docker
    - pip install pyyaml # required by ansible-docker-compose
    # see https://docs.gitlab.com/ee/ci/ssh_keys/#verifying-the-ssh-host-keys
    # about this before_script section
    - mkdir -p ~/.ssh/
    - echo "$SSH_KNOWN_HOSTS" >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    - eval "$(ssh-agent -s)" #start the ssh agent
    # export a few variables based on the current branch
    - chmod +x set_env_vars_based_on_git_branch.sh
    - source ./set_env_vars_based_on_git_branch.sh
  script:
    - cp ansible/ansible.cfg ~/.ansible.cfg
    # install community plugin to manage docker-compose
    - ansible-galaxy collection install community.docker
    # there are no recent releases of anxs.postgresql so we fetch from github directly
    - ansible-galaxy install --roles-path ansible/roles git+https://github.com/ANXS/postgresql.git,63a22322fdb769a4649aea3ff936ce6aea0d963a
    # the name of the role and github repo don't match, we fix it here
    - mv /builds/madada-team/dada-core/ansible/roles/postgresql /builds/madada-team/dada-core/ansible/roles/anxs.postgresql
    - ansible-galaxy install --roles-path ansible/roles geerlingguy.swap
    - make setup-ansible-deps
    # check that the exports were correct
    - echo $DEPLOY_SERVER_NAME
    # ansible cannot get the vault password from an env var
    - echo $ANSIBLE_VAULT_PASSWORD > /tmp/.ansible_vault_pwd
    - export ANSIBLE_VAULT_PASSWORD_FILE=/tmp/.ansible_vault_pwd
    # ansible is very slow at copying the theme over to the host (it does an scp for each of the ~200 files)
    # and using the synchronize module is a pain because it does not handle ssh args the same way as other
    # ansible modules.
    # So we zip the theme here, and then upload that tarball and unpack it on the server
    - tar czf ansible/dftheme.tgz dada-france-theme
    - ansible-playbook -i ansible/ansible_inventory ansible/site.yml --limit $DEPLOY_SERVER_NAME
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    # only deploy the site from one of these 2 branches
    - if: $CI_COMMIT_BRANCH == "master" || $CI_COMMIT_BRANCH == "staging"
      changes:
        - ansible/**/*
        - dada-france-theme/**/*

pages:
  stage: docs
  image: python:latest
  script:
    - pip install -r docs/requirements.txt
    - mkdocs build --site-dir public
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_BRANCH == "doc-public"
      changes:
        - mkdocs.yml
        - docs/**/*

ansible-lint:
  # validate ansible playbook
  stage: test
  image: registry.gitlab.com/pipeline-components/ansible-lint:latest
  script:
    - cd ansible
    - ansible-lint -v --skip-list=var-naming[no-role-prefix] --show-relpath
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_BRANCH
      changes:
        - ansible/**/*

link_cada_directory:
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
  stage: schedule
  image: pfeiffermax/python-poetry:1.5.0-poetry1.5.1-python3.11.4-slim-bookworm
  before_script:
    - apt-get update && apt-get install git -y
    - cd db_sync
    - git clone "https://ci_annuaire_cada:${MY_GITLAB_PAT}@gitlab.com/madada-team/annuaire-cada.git" annuaire_cada
    # Set the displayed user with the commits that are about to be made
    - git config --global user.email "${GIT_USER_EMAIL:-$GITLAB_USER_EMAIL}"
    - git config --global user.name "${GIT_USER_NAME:-$GITLAB_USER_NAME}"
    - cp annuaire_cada/annuaire_prada_web.csv annuaire_cada/hier.csv
    - pwd
    - ls -lR
  after_script:
    # after_script resets cwd to the root of the repo
    - cd db_sync/annuaire_cada
    - git add annuaire_prada_web.csv
    - git add cada_directory_dict.json
    - git add diff.csv
    - |-
      # Check if we have modifications to commit
      CHANGES=$(git status --porcelain | wc -l)

      if [ "$CHANGES" -gt "0" ]; then
        git status
        git commit -m "Mise à jour annuaire PRADA"
        git tag "v$(date -Idate)"
        git push origin "main" -o ci.skip
      fi
  script:
    - poetry install
    - poetry run python dada_db_sync/scrape_prada_directory.py
    - poetry run python dada_db_sync/link_cada.py
    - ls -lR

include:
  # setup automatic secret detection as per
  # https://docs.gitlab.com/ee/user/application_security/secret_detection/
  - template: Security/Secret-Detection.gitlab-ci.yml
