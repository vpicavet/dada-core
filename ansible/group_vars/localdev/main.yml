---
# These variables are for local dev env only, and will not be used
# on any of the servers. Make sure to replicate any additional config
# you add here onto the other group_vars sections as well.

# the alaveteli repo to use
# Note: this is the alaveteli app which is normally fetched from
# https://github.com/mysociety/alaveteli/
# but this setting allows us to use a fork and branch
# for hotfixes or customisations
alaveteli_repo: "https://github.com/mysociety/alaveteli/"
# the branch, tag or commit ref to checkout. Defaults to master
alaveteli_commit: "0.43.1.0"
ruby_version: "3.2.2"

user_support_emails: dada@exemple.fr

# keep emails locally delivered on staging server as we seem to have
# lots of smtp connection issues
# FIXME: change this back to an external address once smpt is fixed
webmaster_email: ubuntu
postmaster_email: postmaster@exemple.fr
contact_madada_forward_emails: "contact@exemple.fr"
dpo_email: dpo@exemple.fr

bourriquet_forward_emails: "contact@exemple.fr"
dadabot_email_address: dadabot@exemple.fr
cada_email_address: fakecada@exemple.fr

db_name_production: alaveteli_production
db_name_test: alaveteli_test
db_user_password: "postgres"

# this one is in vault.yml, encrypted
rails_secret_key_base: "ausfia8fka2u3o823oa8uu3ao3jrao39rjauorha8o3hr"

site_domain: madada.local

# hostname of the server
server_name: dadadev
server_fqdn: "{{ server_name }}.{{ site_domain }}"

incoming_email_secret: "supersecretstuff"

emergency_admin_password: "meanpassword"

http_restricted_username: "devuser"
http_restricted_password: "superpass"

# the recaptcha keys come from https://www.google.com/recaptcha/admin/site/345908267/settings
# the site key can be public, as it appears in the HTML page sent to user browsers
recaptcha_site_key: a8f9ae0f9aefj093ur39uaf903flijfasdfa8f89
recaptcha_secret_key: "captcha secret"

# send all requests to public bodies to this email instead
# useful for staging, set to empty string in production
override_all_public_body_emails: dada@exemple.fr

# change the prefix for staging/production
# to limit confusion
incoming_email_short_prefix: dadadev
incoming_email_prefix: "{{ incoming_email_short_prefix }}+"

# git branch in theme repo to use for the site
# the production site should use master
# You can change this here to work on a different branch
theme_branch: staging

# folder where exim will write incoming emails
raw_emails_location: files/raw_emails

# api token to download the translations
# Any transifex account that has access to them should work
# Do not modify this file, instead copy your token in local.yml
# in the same folder as this file.
transifex_token: "{{ transifex_local_token }}"

# disable db backup on staging server by not specifying any values here
# Do not backup your dev database :)
backup_storage_access_key: ""
backup_storage_secret_key: ""

db_backup_walg_envdir: /etc/wal-g.d/env
db_backup_archive_mode: "off"
db_backup_archive_command: ""
db_backup_storage_endpoint: "https://sos-ch-gva-2.exo.io"
db_backup_storage_prefix: "s3://madada-db-backup"

# files backup, for incoming emails
# you should not need these for local development
files_backup_restic_envdir: /etc/restic/env
files_backup_restic_repository: ""
files_backup_restic_password: "restic-password"

# maxmind license key is used to geolocate IP addresses
# you probably don't need it while developing
maxmind_license_key: "your-maxmind-license-key"

opendkim_private_key: "superprivatekey"

metabase_home: /home/vagrant/metabase
metabase_files_owner: vagrant
metabase_files_group: vagrant
metabase_db_password: "metabasedbpwd"
