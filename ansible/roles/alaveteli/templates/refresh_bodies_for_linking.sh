#!/bin/bash
set -eu

# run this on a daily cron to refresh bodies_for_linker.csv
# which is then used as input to match our db with the list
# of prada scraped from cada.fr

env DB_URL="postgres://{{ db_user_name }}:{{ db_user_password }}@localhost/{{ db_name_production }}" \
bash -c 'psql -d $DB_URL -c "\copy (select pbv.id, pbv.public_body_id, pbv.request_email, pbv.name, pbv.home_page, tags.name as tag_name, nt.body as notes from public_body_versions pbv join public_bodies pb on pbv.public_body_id = pb.id AND pbv.version = pb.version join has_tag_string_tags tags on tags.model_id = pb.id and tags.model_type='"'PublicBody'"' join notes n on n.notable_id=pb.id and n.notable_type='"'PublicBody'"' join note_translations nt on nt.note_id=n.id where tags.value is null order by pbv.created_at desc)  to '{{ alaveteli_path }}/public/restricted/bodies_for_linking.csv' with csv header"'
