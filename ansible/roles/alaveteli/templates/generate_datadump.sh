#!/bin/bash
set -eu

# This script uses pg_dump/pg_restore to make a duplicate of an alaveteli
# database and then runs a sql cleanup script using psql to remove data
# in order to keep only anonymous data.

# The script must be run on a trusted machine as it will access your production
# database and make a full copy of it into a clean database. The final result
# is meant to be publicly shareable, but please consider reviewing the content
# before openly releasing it, as there is no guarantee that no Personally Identifiable
# Information (PII) does not leak through.
# In particular, the following steps are taken to anonymise information, keeping
# in mind that a lot of this information is publicly visible through the corresponding
# website and would make it easy to correlate with in order to deanonymise things.

# This is particularly important with regards to embargoed requests for which we
# should be very careful not to leak any data. The script takes a cautious approach
# and removes any information related to embargoed requests for the time being.

# - all user data is stripped from requests. Anonymised user accounts are kept to allow
#   creating stats about the evolution of number of accounts, etc... It is not possible
#   to link a request to a user however.
# - authors of requests are all set to id 1
# - email content (body and title) is removed. In the future, these will be kept after
#   applying the same censor rules as on the website.
# - censor rules are removed as they obviously contain sensitive data

# HOW TO USE THIS SCRIPT
#
# export LIVE_DB_URL=postgres://username:password@hostname:port/dbname
# export TMP_DB_URL=postgres://username2:pwd@somehost:port/tmpdbname
# ./generate_datadump.sh [-r] <dump_filename.sql>
# WHERE:
# LIVE_DB_URL is the postgres db url to your alaveteli live db (this will only
# be read by pg_dump)
# TMP_DB_URL is the url for the temporary db to use to clean up the dump. The script
# expects this database to have been created (with createdb or `CREATE DATABASE`).
# The database is dropped and recreated before writing to it, to avoid conflicts.
# use the --remove option to delete the temporary db at the end of the process
# dump_filename.sql is the name of the file to which the script will dump the cleaned
# database. If the file exists, it will be overwritten.

# Note that the filesystem on which this script is run must have enough space to
# save a full copy of the live db dump

# export TMP_DB_NAME=dadadump
# export TMP_DB_URL=postgres://postgres:postgres@localhost/dadadump

REMOVE_TMP_DB=false

while getopts 'h?r' OPTION; do
    case "$OPTION" in
        r)
            echo "Will remove the temporary database when done"
            REMOVE_TMP_DB=true
            ;;
        h|\?)
            echo "Usage: $0 [-r] output_filename.sql" >&2
            exit 1
        ;;
    esac
done

shift "$(($OPTIND -1))"

if [ -z "$@" ]; then
    echo "Usage: $0 [-r] output_filename.sql" >&2
    echo "Please provide an output filename to save the db dump to" >&2
    exit 1
else
    echo "Will save output to $1"
    OUTPUT_FILENAME=$1
fi

# Stop if both URLs point to the same db, as this would cause dataloss
[ "$LIVE_DB_URL" = "$TMP_DB_URL" ] && echo "ERROR: TMP_DB_URL points to the LIVE_DB_URL. Stopping." && exit 1

# get the name of the temporary database
TMP_DB_NAME=$(echo $TMP_DB_URL | awk -F '/' '{print $NF}')

LIVE_FULL_DUMP_FILENAME=live_db_tmp_dump.pgdump

echo "Duplicating live database..."
pg_dump -Fc -f $LIVE_FULL_DUMP_FILENAME -d $LIVE_DB_URL

# TODO: create the db as part of this script
# createdb -U postgres $TMP_DB_NAME

echo "Restoring db"
pg_restore --clean --if-exists -Fc -e --no-owner -n public -d $TMP_DB_URL $LIVE_FULL_DUMP_FILENAME

rm $LIVE_FULL_DUMP_FILENAME

# actually do the cleaning
echo "Cleaning up the database..."
psql -d $TMP_DB_URL < {{ metabase_home }}/clean_datadump.sql

echo "Reindexing database..."
psql -d $TMP_DB_URL -c "reindex database $TMP_DB_NAME"

echo "Generating clean SQL dump..."
pg_dump -f $OUTPUT_FILENAME -d $TMP_DB_URL

if [ "$REMOVE_TMP_DB" = "true" ]; then
    echo "Dropping temporary database."
    psql -d $TMP_DB_URL < "DROP SCHEMA public CASCADE; CREATE SCHEMA public;"
fi

echo "All done!"
