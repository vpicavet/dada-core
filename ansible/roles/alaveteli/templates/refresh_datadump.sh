#!/bin/bash
set -eu

# run this on a daily cron to refresh stats.madada.fr
# and the datadump that it uses as datasource.

# dadadump is the cleaned copy of the production database.
# there is also a "metabase" database which holds metabase's metadata, etc...

LIVE_DB_URL="postgres://{{ db_user_name }}:{{ db_user_password }}@localhost/{{ db_name_production }}" \
TMP_DB_URL="postgres://{{ metabase_db_user }}:{{ metabase_db_password}}@localhost/{{ datadump_db_name }}" \
{{ metabase_home }}/generate_datadump.sh "{{ datadump_db_name }}.sql"
