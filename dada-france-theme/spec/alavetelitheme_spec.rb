# If defined, ALAVETELI_TEST_THEME will be loaded in config/initializers/theme_loader
ALAVETELI_TEST_THEME = 'dada-france-theme'

# To run these tests locally (on a dev VM, or somewhere with redis available):
# - alaveteli and this repo are cloned under the same folder (side by side)
# - setup ruby
# - bundle config set --local path 'vendor/bundle'
# - bundle install
# - make sure config/database.yml has the right values for db user/pwd/...
# - copy config/storage.yml from the ansible template
# make sure to specify rails env before each command
# - RAILS_ENV=test bundle exec rails db:test:prepare
# - RAILS_ENV=test bundle exec rails db:migrate
# run the theme tests
# - RAILS_ENV=test bundle exec rspec --format doc lib/themes/dada-france-theme/spec
# or run the full test suite
# - bundle exec rake spec (for some reason this one work without the env set)

require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', '..', '..', 'spec',
                                   'spec_helper'))

describe 'verifying that madada template' do
  it 'tests do indeed work' do
    # this test should always pass, if not there is something
    # wrong with the test setup.
    expect(true).to eq(true)
  end
end

# test cada appeal

# Check that the appeal form is shown

# Verify that the PDF version contains expected email addresses
# Verify that the spreadsheet for a batch request contains expected data
# Verify that a POST on the form shows a preview of appeal email
# A POST on the preview should send out email
# Check target email address for appeal email (cada@...)
describe 'madada:' do
  describe 'when sending an appeal email for a single request' do
    let(:info_request) { FactoryBot.create(:embargoed_request) }
    let(:filename) { info_request.url_title }
    it 'should be sent to the CADA' do
      appeal_email = OutgoingMailer.saisine_cada_automated(
        info_request,
        'pdf_contents',
        'Madame',
        'First',
        'Last',
        nil,
        '30 rue de la gare',
        '75999',
        'Paris',
        'France',
        'les documents',
        'mes commentaires',
        nil
      )
      appeal_email.deliver_now
      info_request.mark_appeal_sent

      expect(appeal_email.text_part.body.to_s).to match(/Votre demande porte sur/)
      expect(appeal_email.to.count).to eq(1)
      expect(appeal_email.to.first).to eq('cada@example.com')
      expect(appeal_email.cc.first).to eq(info_request.user.email)
      expect(appeal_email.attachments.count).to eq(1)
      attachment = appeal_email.attachments[0]
      expect(attachment.body).to eq 'pdf_contents'
      expect(attachment.content_type).to eq('application/pdf')
      expect(attachment.filename).to eq("correspondance_#{filename}.pdf".downcase)

      expect(info_request.described_state).to eq('saisine_cada')
    end
  end

  describe 'when sending an appeal email for a batch request' do
    let(:first_public_body) { FactoryBot.create(:public_body) }
    let(:second_public_body) { FactoryBot.create(:public_body) }
    let(:third_public_body) { FactoryBot.create(:public_body) }
    let(:bodies) { [first_public_body, second_public_body, third_public_body] }
    let!(:info_request_batch) do
      FactoryBot.create(:info_request_batch, title: 'Titre de la série',
                                             body: 'Corps des demandes',
                                             public_bodies: bodies)
    end
    # let!(:first_request) do
    #   FactoryBot.create(:info_request, info_request_batch: info_request_batch,
    #                                    public_body: first_public_body)
    # end
    # let!(:second_request) do
    #   FactoryBot.create(:info_request, info_request_batch: info_request_batch,
    #                                    public_body: second_public_body)
    # end
    # let!(:third_request) do
    #   FactoryBot.create(:info_request, info_request_batch: info_request_batch,
    #                                    public_body: third_public_body)
    # end

    it 'should be sent to the CADA' do
      info_request_batch.create_batch!
      first_request = info_request_batch.info_requests.first
      third_request = info_request_batch.info_requests.last
      expect(first_request.info_request_batch.id).to_not be_nil
      expect(third_request.info_request_batch.id).to_not be_nil

      appeal_email = OutgoingMailer.saisine_cada_automated(
        first_request,
        'batch_pdf_contents',
        'Monsieur',
        'First',
        'Last',
        nil,
        '30 rue de la gare',
        '75999',
        'Paris',
        'France',
        'les documents du batch',
        'mes commentaires du batch',
        # user unchecked third request in form
        [first_request.id, third_request.id]
      )
      appeal_email.deliver_now
      first_request.mark_appeal_sent
      third_request.mark_appeal_sent

      expect(appeal_email.text_part.body.to_s).to match(/Votre demande porte sur/)
      expect(appeal_email.to.count).to eq(1)
      expect(appeal_email.to.first).to eq('cada@example.com')
      expect(appeal_email.cc.first).to eq(first_request.user.email)
      expect(appeal_email.attachments.count).to eq(2)
      attachment1 = appeal_email.attachments[0]
      expect(attachment1.body).to eq 'batch_pdf_contents'
      expect(attachment1.content_type).to eq('application/pdf')
      expect(attachment1.filename).to eq("correspondance_#{first_request.url_title}.pdf")
      attachment2 = appeal_email.attachments[1]
      # expect(attachment2.body).to eq 'pdf_contents'
      expect(attachment2.content_type).to start_with('application/vnd.oasis.opendocument.spreadsheet')
      expect(attachment2.filename).to eq('tableau.ods')

      expect(first_request.described_state).to eq('saisine_cada')
      expect(third_request.described_state).to eq('saisine_cada')
    end
  end
end

# these tests need to run on a dev VM with redis available
RSpec.describe ActsAsXapian::Search do
  describe "#words_to_highlight" do
    before do
      @mtests = FactoryBot.create(:public_body, name: 'Ministère des tests')
      @mairie = FactoryBot.create(:public_body, name: "Mairie d'Ailleurs")
      update_xapian_index
    end

    it "should use the French stemmer" do
      s = ActsAsXapian::Search.new([PublicBody], "chevaux", limit: 100)
      expect(s.words_to_highlight).to eq(%w[cheval])
    end

    it "should match unaccented words" do
      s = ActsAsXapian::Search.new([PublicBody], "ministere tests", limit: 100)
      expect(s.words_to_highlight).to eq(%w[minister test].sort)
    end
  end

  describe '#spelling_correction' do
    before do
      load_raw_emails_data
      update_xapian_index
    end

    before do
      @mairieparis = FactoryBot.create(:public_body, name: 'Mairie de Paris', info_requests_count: 33, tag_string: "mairie")
      @mpnote = FactoryBot.create(:note, body: "Rue de l'hotel de ville", notable: @mairieparis)
      @mairieville = FactoryBot.create(:public_body, name: 'Mairie de Ville', info_requests_count: 4, tag_string: "mairie")
      @mvnote = FactoryBot.create(:note, body: "Rue de paris", notable: @mairieville)
      @ministere = FactoryBot.create(:public_body, name: 'Ministère des Accênts', tag_string: "ministere")
      @interieur = FactoryBot.create(:public_body, name: "Ministère de l'Intérieur", tag_string: "ministere")
      @univparis = FactoryBot.create(:public_body, name: "Université de Paris", tag_string: "universite")
      @trucparis = FactoryBot.create(:public_body, name: "Une administration avec un nom très long qui contient Paris dans son intitulé mais qui doit rester en bas des résultats", tag_string: "autre")
      update_xapian_index
    end

    it 'returns a UTF-8 encoded string' do
      s = ActsAsXapian::Search.new([PublicBody], "mairie velle", limit: 100)
      expect(s.spelling_correction).to eq("mairie ville")
      expect(s.spelling_correction.encoding.to_s).to eq('UTF-8')
    end

    it 'prioritises name over notes' do
      s = ActsAsXapian::Search.new([PublicBody], "ville", limit: 100)
      expect(s.results.length).to eq(2)
      expect(s.results[0][:model].id).to eq(@mairieville.id)
      expect(s.results[1][:model].id).to eq(@mairieparis.id)

      # make sure the reverse search is ranked correctly as well
      s = ActsAsXapian::Search.new([PublicBody], "paris", limit: 100)
      expect(s.results.length).to eq(4)
      # FIXME: this test does not pass, but the same search on the actual site
      # returns the expected results, so it's not clear what breaks here
      expect(s.results[0][:model].id).to eq(@mairieparis.id)
      expect(s.results[1][:model].id).to eq(@univparis.id)
    end

    it 'handles non-ASCII characters' do
      s = ActsAsXapian::Search.new([PublicBody], "ministere", limit: 100)
      expect(s.results.length).to eq(2)
      expect(s.spelling_correction).to eq("ministère")
    end

    it 'finds capitalised public bodies' do
      s = ActsAsXapian::Search.new([PublicBody], "intérieur", limit: 100)
      expect(s.results.length).to eq(1)
      expect(s.results[0][:model].id).to eq(@interieur.id)
    end

  end
end

