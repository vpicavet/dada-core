(function($) {
  const tags = {
    taglist: undefined,

    getList: function() {
      const url = "/api/v2/tags.json";

      if (tags.taglist === undefined) {
        // fetch data from server and cache it
        // (the browser will cache the response as well, so this may not even
        // trigger a network request)
        $.ajax(url).done(function(data) {
          tags.taglist = data;
        });
      }

      return tags.taglist;
    },
    searchByText: function(text) {
      tag_list = tags.getList();
      if (tag_list === undefined) {
        return [];
      }
      return tag_list.filter(function(t) {
        return t.tag.toLowerCase().includes(text.toLowerCase());
      });
    },
  };

  tags.getList();
  var methods = {
    // Initialize our tagging widget:
    // hide the original input (from rails)
    // and add a div which holds each tag as a span
    // plus a new input which is used to type new values
    init: function(options) {
      const originalInput = $(this);
      originalInput.hide();
      originalInput.after(
        `<div class="tagging-tag-container"><span class="tagging-tags-wrapper"></span><input list="suggested-tags" text="" class="tagging-add-tags" name="add-tags" id="add-tags" placeholder="${options.tagInputPlaceholder}" value="" /><datalist id="suggested-tags"></datalist></div>`
      );

      // initialize tags from values provided by backend
      $.each(originalInput[0].value.split(" "), function(_, value) {
        if (value.length < 1) {
          // prevent an empty string from being considered a tag
          return;
        }
        addTag(
          value,
          options.tagBackgroundColor,
          options.tagColor,
          options.tagBorderColor,
          originalInput
        );
      });

      $(".tagging-tag-container").click(function() {
        $(".tagging-add-tags").focus();
        // start loading the list of tags, as it's likely the user
        // is about to type :)
        tags.getList();
      });

      $(".tagging-add-tags").on("keydown", function(evt) {
        if (
          ["Comma", "Alt", "Control", "ArrowDown", "ArrowUp"].includes(evt.key)
        ) {
          return;
        }
        if ([" ", "Tab", "Enter"].includes(evt.key)) {
          // these are "separators" and trigger the addition
          // of a new tag to the list
          var tag = $.trim($(this).val());
          if (tag.length < 1) {
            return false;
          }
          addTag(
            tag,
            options.tagBackgroundColor,
            options.tagColor,
            options.tagBorderColor,
            originalInput
          );
          $(this).val("");
          $(this).focus();
        } else {
          // update the list of suggested tags
          let searchString;
          let suggestedTagsList;

          suggestedTagsList = $("#suggested-tags");
          suggestedTagsList.empty();

          if (evt.key === "Backspace") {
            searchString = $(this).val().slice(0, -1);
          } else {
            searchString = `${$(this).val()}${evt.key}`;
          }

          filteredTags = tags.searchByText(searchString);

          filteredTags.forEach(function(tag) {
            suggestedTagsList.append(`<option value="${tag.tag}" />`);
          });
        }
      });

      // remove the tag when clicking the cross icon
      $(document).on("click", ".tagging-tag-remove", function() {
        var tag = $(this).attr("tag");
        $(`[tag-title='${tag}']`).remove();
        copyTags(originalInput);
        $(".tagging-tags-wrapper").focus();
      });

      return $(".tagging-tag-container").css({
        "border-color": options.tagContainerBorderColor,
        "border-width": ".1em",
        "border-style": "solid",
      });
    },
  };

  // new tags are added as span elements under the div
  function addTag(tagName, tagBgColor, tagColor, tagBorderColor, tagInput) {
    if ($(`[tag-title='${tagName}']`).length === 0) {
      var tagHTML = `<code style="background-color: ${tagBgColor}; color: ${tagColor}; border-color: ${tagBorderColor}" class="tagging-tags" tag-title="${tagName}">
        ${tagName}
        <a title="Remove tag" class="tagging-tag-remove" tag="${tagName}">
            <span class="tagging-icon-close"></span>
        </a>
        </code>`;
      $(".tagging-tags-wrapper").append(tagHTML);
      copyTags(tagInput);
    }
  }

  // copy tags to the original input as a string
  // to respect the original logic, so that validating the form
  // still works with rails
  function copyTags(tagInput) {
    var listOfTags = [];
    $(".tagging-tags").each(function() {
      listOfTags.push($(this).text().trim());
    });
    tagInput.val(listOfTags.join(" "));
  }

  $.fn.TagsWidget = function(methodOrOptions) {
    if (typeof methodOrOptions === "object" || !methodOrOptions) {
      return methods.init.apply(this, arguments);
    } else if (methods[methodOrOptions]) {
      return methods[methodOrOptions].apply(
        this,
        Array.prototype.slice.call(arguments, 1)
      );
    } else {
      $.error("Invalid method " + methodOrOptions + " for TagsWidget.");
    }
  };
})(jQuery);
