# Add a callback - to be executed before each request in development,
# and at startup in production - to patch existing app classes.
# Doing so in init/environment.rb wouldn't work in development, since
# classes are reloaded, but initialization is not run each time.
# See http://stackoverflow.com/questions/7072758/plugin-not-reloading-in-development-mode
#
Rails.configuration.to_prepare do
  # Example adding an instance variable to the frontpage controller
  # GeneralController.class_eval do
  #   def mycontroller
  #     @say_something = "Greetings friend"
  #   end
  # end
  # Example adding a new action to an existing controller
  # HelpController.class_eval do
  #   def help_out
  #   end
  # end

  def forum; end
  def stats; end
  def madada_blog; end

  RequestController.class_eval do
    # Patch RequestController with our custom action for madada
    # This adds a new method on the class.
    # Note that the rendered file is cached, so clear the cached version
    # if you update this code!
    def download_entire_request_saisine
      @info_request = InfoRequest.find_by_url_title!(params[:url_title])
      # Check for access and hide embargoed requests immediately, so that we
      # don't leak any info to people who can't access them
      render_hidden if @info_request.embargo && cannot?(:read, @info_request)
      if authenticated?
        # Test for whole request being hidden or requester-only
        return render_hidden if cannot?(:read, @info_request)

        cache_file_path = @info_request.make_zip_cache_path_for_saisine(@user)
        unless File.exist?(cache_file_path)
          FileUtils.mkdir_p(File.dirname(cache_file_path))
          make_request_zip_for_saisine(@info_request, cache_file_path)
          File.chmod(0o644, cache_file_path)
        end
        send_file(cache_file_path, filename: "saisine_cada_#{params[:url_title]}.zip")
      else
        ask_to_login(
          web: _('To download the zip file'),
          email: _('Then you can download a zip file of ' \
                   '{{info_request_title}}.',
                   info_request_title: @info_request.title),
          email_subject: _('Log in to download a zip file of ' \
                           '{{info_request_title}}',
                           info_request_title: @info_request.title)
        )
      end
    end

    def make_request_zip_for_saisine(info_request, file_path)
      Zip::File.open(file_path, create: true) do |zipfile|
        file_info = make_request_summary_file_for_saisine(info_request)
        zipfile.get_output_stream(file_info[:filename]) { |f| f.write(file_info[:data]) }
        message_index = 0
        info_request.incoming_messages.each do |message|
          next unless can?(:read, message)

          message_index += 1
          message.get_attachments_for_display.each do |attachment|
            filename = "#{message_index}_#{attachment.url_part_number}_#{attachment.display_filename}"
            zipfile.get_output_stream(filename) do |f|
              body = message.apply_masks(attachment.default_body, attachment.content_type)
              f.write(body)
            end
          end
        end
      end
    end

    def make_request_summary_file_for_saisine(info_request)
      done = false
      @render_to_file = true
      assign_variables_for_show_template(info_request)
      if HTMLtoPDFConverter.exist?
        html_output = render_to_string(template: 'request/show', variant: :cada)
        tmp_input = Tempfile.new(['foihtml2pdf-input', '.html'])
        tmp_input.write(html_output)
        tmp_input.close
        tmp_output = Tempfile.new('foihtml2pdf-output')
        command = HTMLtoPDFConverter.new(tmp_input, tmp_output)
        output = command.run
        if output.nil?
          logger.error("Could not convert info request #{info_request.id} to PDF with command '#{command}'")
        else
          file_info = { filename: "correspondance_#{info_request.url_title}.pdf",
                        data: File.read(tmp_output.path) }
          done = true
        end
        tmp_output.close
        tmp_input.delete
        tmp_output.delete
      else
        logger.warn('No HTML -> PDF converter found')
      end
      unless done
        file_info = { filename: 'correspondance.txt',
                      data: render_to_string(template: 'request/show',
                                             layout: false,
                                             formats: [:text]) }
      end
      file_info
    end

    # Send an ODS file containing the data required by the CADA for a batch request appeal
    # Do not cache this file, as the variety of events that would lead to clearing
    # the cache is too large.
    # It is not meant to be downloaded more than once in reality anyway.
    # This method only saves the file locally.
    def generate_batch_saisine_spreadsheet
      @info_request = InfoRequest.find_by_url_title!(params[:url_title])
      file_data = @info_request.create_saisine_spreadsheet

      tmp_file = Tempfile.new(['saisine_serielle', '.ods'])
      File.binwrite(tmp_file, file_data)
      tmp_file
    end

    # Send the file produced above back to the client
    def download_batch_saisine_spreadsheet
      filename = "tableau_saisine_cada_#{params[:url_title]}.ods"
      send_file(generate_batch_saisine_spreadsheet, filename: filename)
    end

    # Request additional info from the user, then send the CADA
    # an appeal email with relevant attachments
    def send_cada_appeal
      @info_request = InfoRequest.find_by_url_title!(params[:url_title])

      # restrict access to requester and admins only
      if !(@user == @info_request.user || can?(:admin, @info_request))
        render_hidden
        return
      end

      if params[:commit].nil?
        render action: 'send_cada_appeal'
        return
      end

      # respond to POST verb, and send the appeal doc
      # TODO: add an email preview step as powerusers might want to check
      # what we did for them

      # prerender the pdf file as it's very hard to do outside the controller
      pdf_file = make_request_summary_file_for_saisine(@info_request)[:data]

      OutgoingMailer.saisine_cada_automated(
        @info_request,
        pdf_file,
        params[:user_title],
        params[:user_firstname],
        params[:user_lastname],
        params[:on_behalf_of],
        params[:user_post_address],
        params[:user_postcode],
        params[:user_city],
        params[:user_country],
        params[:docs_requested],
        params[:observations],
        params[:reqs_to_include],
      ).deliver_now

      # update selected requests states
      if @info_request.info_request_batch.nil?
        @info_request.mark_appeal_sent
      else
        if params[:reqs_to_include].nil?
          reqs = @info_request.info_request_batch&.info_requests
        else
          # prepend the id of the current request as the html form does not include it
          # when the checkbox is disabled
          reqs = @info_request.info_request_batch&.info_requests.where("id IN (?)", params[:reqs_to_include].prepend(@info_request.id))
        end
        reqs.each do |ir|
          ir.mark_appeal_sent
        end
      end

      # redirect to the request page, scrolled to the latest event
      redirect_to show_request_path and return
    end

    module InfoRequestExtensions
      def show
        # themes copied from the list that the CADA uses (see their opendata page)
        @theme_values = [
          ['Justice', 'justice'],
          ['Affaires Sanitaires Et Sociales', 'affaires_sanitaires_et_sociales'],
          ['Travail Et Emploi', 'travail_et_emploi']
        ]

        # prepare the email contents for the CADA appeal
        mailer = OutgoingMailer.new
        @saisine_email_contents = mailer.saisine_cada(@info_request)

        super
      end
    end

    # see https://stackoverflow.com/a/4471202/1138710
    # about how this works
    prepend InfoRequestExtensions
  end

  PublicBodyChangeRequestsController.class_eval do
    # turn off this before_action that is set in ApplicationController
    # so that rails will respond to non-html requests for the create endpoint
    skip_before_action :html_response, only: :create

    # Override the default action to handle JSON requests as well
    # with no captcha
    def create
      @change_request =
        PublicBodyChangeRequest
        .from_params(params[:public_body_change_request], @user)

      respond_to do |format|
        # keep the standard flow for html requests
        format.html do
          verified =
            if @render_recaptcha
              recaptcha_args = {
                model: @change_request,
                message: _('There was an error with the reCAPTCHA. ' \
                           'Please try again.')
              }
              verify_recaptcha(recaptcha_args)
            else
              true
            end

          if verified && @change_request.save
            @change_request.send_message
            redirect_to frontpage_url, notice: @change_request.thanks_notice
          else
            render action: 'new'
          end
        end

        format.json do
          # bypass captchas for json/api calls
          if @change_request.save
            render json: {
              success: 'Demande enregistrée'
            }
          end
        end
      end
    end
  end

  AdminGeneralController.class_eval do
    alias_method :original_index, :index

    # override original method to add list of requests that
    # require solving a captcha
    def index
      original_index
      captchas_query = InfoRequest
                       .joins(<<-SQL1)
                        JOIN LATERAL (
                          SELECT im.* from incoming_messages im
                          WHERE im.info_request_id = info_requests.id
                          ORDER BY im.id DESC
                          LIMIT 1
                        ) AS last_im ON true
                       SQL1
                       .joins(<<-SQL2)
                        INNER JOIN has_tag_string_tags t
                        ON t.model_type = 'IncomingMessage'
                        AND t.model_id = last_im.id
                       SQL2
                       .where(awaiting_description: true)
                       .where("t.name='captcha' AND t.value='notification'")
      @antispam_captchas_count = captchas_query.count
      @antispam_captchas = captchas_query.limit(20)

      # rewrite unclassified list as the most recent ones
      @old_unclassified = InfoRequest.where_old_unclassified
                                     .order(updated_at: :desc)
                                     .limit(20)
                                     .is_searchable
    end
  end

  AdminPublicBodyController.class_eval do
    def edit
      @public_body.build_all_translations
      @hide_destroy_button = @public_body.info_requests.count > 0

      if params[:change_request_id]
        @change_request = PublicBodyChangeRequest.find(params[:change_request_id])
      end

      if @change_request
        @change_request_user_response =
          render_to_string(
            template: 'admin_public_body_change_requests/update_accepted',
            formats: [:text]
          )
        @public_body.request_email = @change_request.public_body_email
        @public_body.last_edit_comment = @change_request.comment_for_public_body
      else
        @public_body.last_edit_comment = ''
      end

      @cada_data = @public_body.load_cada_data

      render formats: [:html]
    end
  end

  ApiController.class_eval do
    skip_before_action :check_api_key, only: [:list_tags]

    # list available tags with their use count and the model
    # they apply to. Used by tagging widget
    def list_tags
      query = <<-SQL
        select concat_ws(':',  name, value) as tag,
          count(*) as cnt,
          case
            when model_type='PublicBody' then 'pb'
            when model_type='InfoRequest' then 'ir'
            when model_type='IncomingMessage' then 'im'
            when model_type='OutgoingMessage' then 'om'
            else 'xx'
          end as tag_type
        from has_tag_string_tags
        group by (name, value, model_type)
        order by cnt desc;
      SQL

      render json: ActiveRecord::Base.connection.execute(query)
    end
  end
end
