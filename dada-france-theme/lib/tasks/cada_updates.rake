namespace :madada_cada_updates do
  desc 'Create change requests based on CADA directory updates'
  # list bodies which have changed in our daily diff file, and create
  # a change request for each of them.
  task create_change_requests: :environment do
    url = 'https://gitlab.com/madada-team/annuaire-cada/-/raw/main/diff.csv?ref_type=heads&inline=false'
    dadabot_user = User.find_by_email(AlaveteliConfiguration.dadabot_email_address)
    csv = CSV.open(URI.parse(url).open, headers: true)
    csv.each do |row|
      next if row.fetch('id_madada').nil?
      next if row.fetch('_merge') != 'new'

      puts "Change Request for #{row.field('id_madada')} url #{row.fetch('url_cada')}"
      cr = PublicBodyChangeRequest.new
      cr.public_body_id = row.fetch('id_madada')
      cr.public_body_name = row.fetch('nom')
      cr.public_body_email = row.fetch('email')
      cr.notes = row.fetch('adresse')
      cr.user_id = dadabot_user.id
      cr.source_url = "https://cada.fr#{row.fetch('url_cada')}"
      cr.save
    end
  end

  desc 'Request addition of new bodies based on CADA directory updates'
  # list bodies which appear in annuaire_prada_web.csv but not in
  # cada_directory_dict.json. Those are probably missing from our db.
  # Request creation of each of them.
  task create_missing_bodies: :environment do
    csv_url = 'https://gitlab.com/madada-team/annuaire-cada/-/raw/main/annuaire_prada_web.csv?ref_type=heads&inline=false'
    json_url = 'https://gitlab.com/madada-team/annuaire-cada/-/raw/main/cada_directory_dict.json?ref_type=heads&inline=false'
    dadabot_user = User.find_by_email(AlaveteliConfiguration.dadabot_email_address)

    all_pradas = CSV.open(URI.parse(csv_url).open, headers: true)

    all_matched_pradas = JSON.load(URI.parse(json_url).open)
    matched_pradas = all_matched_pradas.values.map { |p| p['url_cada'] }

    tot = 0
    all_pradas.each do |prada|
      next if matched_pradas.include?(prada.fetch('url_id'))
      next if prada.fetch('url_id').include?('prefecture')
      next if prada.fetch('url_id').include?('agglomeration')
      next if prada.fetch('url_id').include?('ministere')
      next if prada.fetch('url_id').include?('conseil-departemental')
      next if prada.fetch('url_id').include?('conseil-regional')

      tot += 1

      puts "Creating #{prada.fetch('url_id')}"
      cr = PublicBodyChangeRequest.new
      cr.public_body_name = prada.fetch('nom')
      cr.public_body_email = prada.fetch('email')
      cr.notes = "Adresse : #{prada.fetch('adresse')}"
      cr.user_id = dadabot_user.id
      cr.source_url = "https://cada.fr#{prada.fetch('url_id')}"
      cr.save
    end
    puts "Total bodies created: #{tot}"
  end
end
