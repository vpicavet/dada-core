# Methods related to sending custom reminders for Ma Dada
Rails.configuration.to_prepare do
  RequestMailer.class_eval do
    # send a reminder to the public body with details about the request they
    # still have not replied to.
    def remind_one_week_before_deadline(info_request, email)
      @info_request = info_request
      @contact_email = AlaveteliConfiguration.contact_email

      mail(to: email,
           from: info_request.incoming_name_and_email,
           reply_to: info_request.incoming_name_and_email,
           subject: 'Il vous reste une semaine pour répondre à '\
                    'une demande d\'accès à un document administratif - '\
                    "#{@info_request.title}")
    end
  end
end
