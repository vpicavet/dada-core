Rails.configuration.to_prepare do
  OutgoingMailer.class_eval do
    # render the contents of the saisine template as a string
    # which will be shown as a mailto: link
    def saisine_cada(info_request)
      render_to_string 'saisine_cada', format: :text, locals: { info_request: info_request }
    end

    def saisine_cada_automated(
      info_request,
      pdf_file,
      user_title,
      user_firstname,
      user_lastname,
      on_behalf_of,
      user_post_address,
      user_postcode,
      user_city,
      user_country,
      docs_requested,
      observations,
      reqs_to_include
    )
      @info_request = info_request
      @user_title = user_title
      @user_firstname = user_firstname
      @user_lastname = user_lastname
      @on_behalf_of = on_behalf_of
      @user_post_address = user_post_address
      @user_postcode = user_postcode
      @user_city = user_city
      @user_country = user_country
      @docs_requested = docs_requested
      @observations = observations
      @outgoing_message = OutgoingMessage.new(info_request: @info_request)

      # attach the rendered PDF
      attachments["correspondance_#{info_request.url_title}.pdf"] =
        { mime_type: 'application/pdf',
          content: pdf_file }

      unless info_request.info_request_batch.nil?
        attachments['tableau.ods'] =
          info_request.create_saisine_spreadsheet(reqs_to_include)
      end

      mail(from: @outgoing_message.from,
           to: AlaveteliConfiguration.cada_email_address,
           cc: @info_request.user.email,
           subject: 'CADA : Formulaire de saisine via MaDada.fr')
    end
  end
end
