# Add a callback - to be executed before each request in development,
# and at startup in production - to patch existing app classes.
# Doing so in init/environment.rb wouldn't work in development, since
# classes are reloaded, but initialization is not run each time.
# See http://stackoverflow.com/questions/7072758/plugin-not-reloading-in-development-mode
#
# require various files just to make it a bit easier to organise code
require 'reminders'

Rails.configuration.to_prepare do
  # Example of adding a default text to each message
  # OutgoingMessage.class_eval do
  #   # Add intro paragraph to new request template
  #   def default_letter
  #     # this line allows the default_letter text added by this
  #     # method to be replaced by the value supplied by the API
  #     # e.g. http://demo.alaveteli.org/new/tgq?default_letter=this+is+a+test
  #     return @default_letter if @default_letter
  #     return nil if self.message_type == 'followup'
  #     "If you uncomment this line, this text will appear as default text in every message"
  #   end
  # end

  AlaveteliConfiguration.class_eval do
    # Allow additional config keys to be defined. This is a bit clunky
    # but allows using the regular config file for this, and our ansible
    # system to pass config in a secure manner
    def self.dadabot_email_address
      MySociety::Config.get('DADABOT_EMAIL_ADDRESS', 'bot@example.com')
    end

    def self.cada_email_address
      MySociety::Config.get('CADA_EMAIL_ADDRESS', 'cada@example.com')
    end
  end

  ApplicationHelper.class_eval do
    # These are used in search result listings for instance. Overridden here
    # to hide user names
    def event_description(event)
      body_link = public_body_link_absolute(event.info_request.public_body)
      user_link = request_user_link_absolute(event.info_request)
      date = simple_date(event.created_at)
      case event.event_type
      # cases below use an ugly hack by trying to translate a non existing sentence
      # this returns the same. Somehow, formatting the sentence directly via interpolation
      # breaks all links.
      when 'sent'
        _('Demande envoyée à {{public_body_name}} le {{date}}.',
          public_body_name: body_link,
          info_request_user: user_link,
          date: date)
      when 'followup_sent'
        case event.calculated_state
        when 'internal_review'
          _('Recours amiable envoyé à {{public_body_name}} le {{date}}.',
            public_body_name: body_link,
            info_request_user: user_link,
            date: date)
        when 'waiting_response'
          _('Clarification envoyée à {{public_body_name}} le {{date}}.',
            public_body_name: body_link,
            info_request_user: user_link,
            date: date)
        else
          _('Message de suivi envoyé à {{public_body_name}} le {{date}}.',
            public_body_name: body_link,
            info_request_user: user_link,
            date: date)
        end
      when 'response'
        _('Réponse de {{public_body_name}} le {{date}}.',
          public_body_name: body_link,
          info_request_user: user_link,
          date: date)
      when 'comment'
        _('Demande envoyée à {{public_body_name}}. Commentée le {{date}}.',
          public_body_name: body_link,
          info_request_user: user_link,
          event_comment_user: user_link_absolute(event.comment.user),
          date: date)
      end
    end
  end

  InfoRequest.class_eval do
    def make_zip_cache_path_for_saisine(user)
      # This method is only used for the saisine CADA PDF doc.
      # The filename needs to be different from the "regular" PDF as the content
      # of this one is different. This prevents the cache from returning the wrong
      # file, should users request both files.
      # The zip file varies depending on user because it can include different
      # messages depending on whether the user can access hidden or
      # requester_only messages. We name it appropriately, so that every user
      # with the right permissions gets a file with only the right things in.
      cache_file_dir = File.join(InfoRequest.download_zip_dir,
                                 'download',
                                 request_dirs,
                                 last_update_hash)
      cache_file_suffix = zip_cache_file_suffix(user)
      File.join(cache_file_dir, "saisine_#{url_title}#{cache_file_suffix}.zip")
    end

    # assemble an openDocument spreadsheet containing the following columns for each
    # request that was part of the batch the request is part of:
    # - name of the targeted entity
    # - request email address
    # - date at which the request was last sent
    #   (we assume that last time it was sent was successful)
    # - date of explicit rejection by the authority (skipped for now)
    # Do not cache this file, as the variety of events that would lead to
    # clear the cache is too large.
    # It is not meant to be downloaded more than once in reality anyway.
    def create_saisine_spreadsheet(ids_to_include = nil)
      data = []
      unless info_request_batch.nil?
        if ids_to_include.nil?
          reqs = info_request_batch&.info_requests
        else
          # prepend the id of the current request as the html form does not include it
          # when the checkbox is disabled
          reqs = info_request_batch&.info_requests.where("id IN (?)", ids_to_include.prepend(id))
        end
        data = reqs.map do |ir|
          [
            ir.public_body.name,
            ir.public_body.request_email,
            ir.date_initial_request_last_sent_at,
            (ir.last_public_response_at&.strftime('%d-%m-%Y') if ir.described_state == 'rejected')
          ]
        end
      end
      headers = ["Nom de l'administration",
                 'Adresse électronique',
                 'Date de réception de la demande de communication',
                 "Date de notification du refus explicite par l'administration (le cas échéant)"]
      SpreadsheetArchitect.to_ods(headers: headers, data: data)
    end

    def mark_appeal_sent
      # update the request state and save an event
      log_event(
        'status_update',
        user_id: user.id,
        old_described_state: described_state,
        described_state: 'saisine_cada',
        message: "Saisine envoyée à la CADA"
      )
      # change status to error in delivery
      # updates the state, triggers email notification "someone updated your request"
      set_described_state(
        'saisine_cada',
        user,
        "Saisine envoyée à la CADA"
      )
    end
  end

  IncomingMessage.class_eval do
    # Add a class method which will select recently received
    # messages, and apply some heuristics to automatically
    # classify the ones that are easy.
    def self.filter_new_emails(how_long_ago = 10.minute)
      # use a 10 minute time window to make sure we don't miss messages.
      # We can safely run this code twice on a single message as it won't have any
      # weird side effects. This filter is expected to run every 5 minutes.
      n_minutes_ago = Time.zone.now - how_long_ago

      dadabot_user = User.find_by_email(AlaveteliConfiguration.dadabot_email_address)
      notif_tag_string = 'captcha:notification'

      # reduce the default batch size from 1000 to 100 to limit
      # memory errors while processing large batches
      where(
        ['incoming_messages.created_at > ?', n_minutes_ago]
      ).joins(
        :info_request
      ).where(
        'info_requests.awaiting_description=true'
      ).find_each(batch_size: 100) do |email|
        sender_email = email.from_email.downcase

        # email delivery error messages
        if sender_email.start_with?('postmaster@') ||
           sender_email.start_with?('mailer-daemon') ||
           (sender_email.start_with?('mdaemon@') &&
            email.subject.downcase.include?('utilisateur inconnu'))

          ir = email.info_request
          logger.info "Marking request #{ir.id} as having an email_error"
          # log an event that is visible in the request's admin page
          ir.log_event(
            'status_update',
            user_id: dadabot_user.id,
            old_described_state: ir.described_state,
            described_state: 'error_message',
            message: "[DadaBot] Message d'erreur reçu du serveur destinataire"
          )
          # change status to error in delivery
          # updates the state, triggers email notification "someone updated your request"
          ir.set_described_state(
            'error_message',
            dadabot_user,
            "Message d'erreur reçu du serveur destinataire"
          )
        end

        # captcha notifications
        # After the email is tagged, the admin interface will show corresponding
        # requests under a specific section.
        if (sender_email.include?('@mailinblack') ||
           sender_email.include?('@invitations.mailinblack.com') ||
           sender_email =~ /@antispam.*.xefi.fr/) &&
           !(email.has_tag?('captcha') &&
             email.get_tag_values('captcha').include?('notification'))
          logger.info "Tagging incoming_message #{email.id} with #{notif_tag_string}"
          email.add_tag_if_not_already_present(notif_tag_string)
          email.info_request.log_event(
            'edit',
            user_id: dadabot_user.id,
            message: 'Notification captcha antispam identifiée'
          )
        end
      end
    end

    def custom_text_for_admins
      text = get_main_body_text_internal
      # Strip the uudecode parts from main text
      # - this also effectively does a .dup as well, so text mods don't alter original
      text = text.split(/^begin.+^`\n^end\n/m).join(' ')

      if text.size > 1_000_000 # 1 MB ish
        raise 'main body text more than 1 MB, need to implement clipping like for attachment text, or there is some other MIME decoding problem or similar'
      end

      # apply masks for this message
      apply_masks(text, 'text/html', false)
    end

    def custom_text_folded_for_admins
      text = custom_text_for_admins
      # Remove existing quoted sections
      folded_quoted_text = remove_lotus_quoting(text, 'FOLDED_QUOTED_SECTION')
      folded_quoted_text = IncomingMessage.remove_quoted_sections(folded_quoted_text,
                                                                  'FOLDED_QUOTED_SECTION')
      folded_quoted_text.delete("\0")
    end

    def custom_text_unfolded_for_admins
      text = custom_text_for_admins
      text.delete("\0")
    end

    def get_main_body_text_folded(hide_all_emails = true)
      if hide_all_emails
        # same behaviour as upstream alaveteli
        _cache_main_body_text if cached_main_body_text_folded.nil?
        cached_main_body_text_folded
      else
        # bypass caching mechanism for admins (and possibly requester)
        custom_text_folded_for_admins
      end
    end

    def get_main_body_text_unfolded(hide_all_emails = true)
      if hide_all_emails
        # same behaviour as upstream alaveteli
        _cache_main_body_text if cached_main_body_text_unfolded.nil?
        cached_main_body_text_unfolded
      else
        # bypass caching mechanism for admins (and possibly requester)
        custom_text_unfolded_for_admins
      end
    end

    def apply_masks(text, content_type, hide_all_emails = true)
      # override the standard method to skip censoring if not required
      mask_options = { censor_rules: info_request.applicable_censor_rules,
                       masks: info_request.masks,
                       hide_all_emails: hide_all_emails }
      AlaveteliTextMasker.apply_masks(text, content_type, mask_options)
    end

    def get_body_for_html_display(collapse_quoted_sections = true, hide_all_emails = true)
      # text content of emails is cached once for all users. We bypass the cache for
      # admins, to make sure we don't accidentally cache an uncensored version of the
      # text.
      # This overridden method just adds hide_all_emails argument to the 2 calls below
      # Find the body text and remove emails for privacy/anti-spam reasons
      text = get_main_body_text_unfolded(hide_all_emails)
      folded_quoted_text = get_main_body_text_folded(hide_all_emails)

      # Remove quoted sections, adding HTML. TODO: The FOLDED_QUOTED_SECTION is
      # a nasty hack so we can escape other HTML before adding the unfold
      # links, without escaping them. Rather than using some proper parser
      # making a tree structure (I don't know of one that is to hand, that
      # works well in this kind of situation, such as with regexps).
      text = folded_quoted_text if collapse_quoted_sections
      text = MySociety::Format.simplify_angle_bracketed_urls(text)
      text = CGI.escapeHTML(text)
      text = MySociety::Format.make_clickable(text, contract: 1)

      # add a helpful link to email addresses and mobile numbers removed
      # by apply_masks
      email_pattern = Regexp.escape(_('email address'))
      mobile_pattern = Regexp.escape(_('mobile number'))
      text.gsub!(/\[(#{email_pattern}|#{mobile_pattern})\]/,
                 '[<a href="/help/officers#mobiles">\1</a>]')

      if collapse_quoted_sections
        text = text.gsub(/(\s*FOLDED_QUOTED_SECTION\s*)+/m, 'FOLDED_QUOTED_SECTION')
        text.strip!
        # if there is nothing but quoted stuff, then show the subject
        if text == 'FOLDED_QUOTED_SECTION'
          text = '[Subject only] ' + CGI.escapeHTML(subject || '') + text
        end
        # and display link for quoted stuff
        text = text.gsub('FOLDED_QUOTED_SECTION',
                         "\n\n" + '<span class="unfold_link"><a href="?unfold=1#incoming-' + id.to_s + '">' + _('show quoted sections') + '</a></span>' + "\n\n")
      elsif folded_quoted_text.include?('FOLDED_QUOTED_SECTION')
        text = text + "\n\n" + '<span class="unfold_link"><a href="?#incoming-' + id.to_s + '">' + _('hide quoted sections') + '</a></span>'
      end
      text.strip!

      text = ActionController::Base.helpers.simple_format(text)
      text.html_safe
    end
  end

  InfoRequestHelper.class_eval do
    # Override the default text in the red banner in the header of a request view
    # to replace internal review with cada appeal instead.
    def status_text_waiting_response_very_overdue(info_request, _opts = {})
      str = _('Response to this request is <strong>long overdue</strong>.')
      str += ' '
      str += if info_request.public_body.not_subject_to_law?
               _('Although not legally required to do so, we would have ' \
                 'expected {{public_body_link}} to have responded by now',
                 public_body_link: public_body_link(info_request.public_body))
             else
               "Selon la loi, l'administration aurait déjà dû répondre"
             end
      str += ' '

      unless info_request.is_external?
        str += ' '
        str += 'Si vous avez envoyé la demande initiale, et si les délais le permettent encore, vous pouvez '
        str += link_to 'saisir la CADA',
                       'https://doc.madada.fr/usagers/procedure-de-demande/#recours-administratif-saisine-cada'
        # str += link_to 'envoyer une saisine à la CADA',
        # new_request_followup_path(request_id: info_request.id) +
        # '?internal_review=1'
        str += '.'
      end

      str
    end
  end

  AlaveteliTextMasker.class_eval do
    # override the default method to move email censor rules out
    # to the added method below
    def default_text_masks
      [{ to_replace: %r{(Mobile|Mob)([\s/]*(Fax|Tel))*\s*:?[\s\d]*\d},
         replacement: "[#{_('mobile number')}]" },
       { to_replace: %r{https?://#{AlaveteliConfiguration.domain}/c/[^\s]+},
         replacement: "[#{_('{{site_name}} login link',
                            site_name: site_name)}]" }]
    end

    def hide_all_emails_masks
      [{ to_replace: MySociety::Validate.email_find_regexp,
         replacement: "[#{_('email address')}]" }]
    end

    def apply_text_masks(text, options = {})
      # override the upstream method to make it possible to skip censoring
      # all emails by default.
      masks = options[:masks] || []
      masks += default_text_masks
      masks += hide_all_emails_masks if options[:hide_all_emails]
      censor_rules = options[:censor_rules] || []

      text = masks.inject(text) do |memo, mask|
        memo.gsub(mask[:to_replace], mask[:replacement])
      end

      censor_rules.reduce(text) { |t, rule| rule.apply_to_text(t) }
    end
  end

  OutgoingMessage::Template::InitialRequest.class_eval do
    # adjust the request template to remove too many \n before signature
    def template_string(replacements)
      msg = salutation(replacements)
      msg += letter(replacements)
      msg += "\n\n"
      msg += signoff(replacements)
      msg + "\n\n"
    end
  end

  #######################################################################
  # customise xapian indexing for better search
  #######################################################################
  # acts_as_xapian is passed:
  # texts: the fields of the model that need to be indexed, with a weight:
  #        higher numbers mean more important information
  User.class_eval do
    acts_as_xapian texts: [[:name, 10], [:about_me, 100]],
                   values: [
                     [:created_at_numeric, 1, 'created_at', :number] # for sorting
                   ],
                   terms: [[:variety, 'V', 'variety']],
                   if: :indexed_by_search?
  end

  InfoRequestEvent.class_eval do
    acts_as_xapian \
      texts: [
        [:search_text_main, 100],
        [:title, 100],
      ],
      values: [
        # for QueryParser range searches e.g. 01/01/2008..14/01/2008:
        [:created_at, 0, 'range_search', :date],
        # for sorting:
        [:created_at_numeric, 1, 'created_at', :number],
        # TODO: using :number for lack of :datetime support in Xapian values:
        [:described_at_numeric, 2, 'described_at', :number],
        [:request, 3, 'request_collapse', :string],
        [:request_title_collapse, 4, 'request_title_collapse', :string]
      ],
      terms: [
        [:calculated_state, 'S', 'status'],
        [:requested_by, 'B', 'requested_by'],
        [:requested_from, 'F', 'requested_from'],
        [:commented_by, 'C', 'commented_by'],
        [:request, 'R', 'request'],
        [:variety, 'V', 'variety'],
        [:latest_variety, 'K', 'latest_variety'],
        [:latest_status, 'L', 'latest_status'],
        [:waiting_classification, 'W', 'waiting_classification'],
        [:filetype, 'T', 'filetype'],
        [:tags, 'U', 'tag'],
        [:request_public_body_tags, 'X', 'request_public_body_tag']
      ],
      eager_load: [
        :outgoing_message,
        :comment,
        { info_request: [:user, :public_body, :censor_rules] }
      ],
      if: :indexed_by_search?
  end

  PublicBody.class_eval do
    # fetch the linked json dict from local file
    # and cache it in memory

    # load data from the CADA directory after it has been processed nightly
    # by our monitoring tool
    def self.linked_cada_directory
      Rails.cache.fetch('cada_directory', expires_in: 3.hours) do
        url = 'https://gitlab.com/madada-team/annuaire-cada/-/raw/main/cada_directory_dict.json?ref_type=heads&inline=false'
        JSON.parse(URI.parse(url).open.read, { allow_nan: true })
      end
    end

    def load_cada_data
      PublicBody.linked_cada_directory[id.to_s]
    end

    def name_for_search
      # special-case this one as the stemmer seems to get confused by the combination
      # of uppercase and l' prefix
      if name == "Ministère de l'Intérieur"
        return "ministère intérieur"
      end
      return name.downcase
    end

    def weighted_name_for_category
      # define a weight for the name field based on the category of the public body
      # so that "more important" ones come up first in results.
      # but: if default weight is 1000, mairie paris comes up before uni paris
      # but if default is 7000, then uni comes up first again, why?
      # notes are weighted less for a lower value here
      # best results so far with 800 as default weight
      w = 800
      tafs = tag_array_for_search
      if tafs.include? "mairie"
        w = 1800
      elsif tafs.include? "epci"
        w = 1800
      elsif tafs.include? "ministere"
        w = 2000
      elsif tafs.include? "cg"
        w = 2000
      elsif tafs.include? "prefecture"
        w = 2000
      end
      w
    end

    acts_as_xapian texts: [
                            [:name_for_search,  :weighted_name_for_category],
                            [:short_name,  500],
                            [:notes_as_string, 50]
                          ],
                 values: [
                   # used for specific sorting, has no impact on the default
                   # order is "sortBy is not specified
                   [:info_requests_count, 5, "nb_demandes", :number],
                   [:created_at_numeric, 1, "created_at", :number]
                 ],
                 terms: [
                   [:name_for_search, 'N', 'name'],
                   [:variety, 'V', "variety"],
                   [:tag_array_for_search, 'U', "tag"]
                 ],
                 eager_load: [:translations]
  end

  PublicBodyCategoryLink.class_eval do
    # override the default method to add a count of bodies for each tag
    def self.by_display_order
      headings_table = Arel::Table.new(:public_body_headings)
      links_table = Arel::Table.new(:public_body_category_links)

      PublicBodyCategoryLink
        .select(headings_table[:display_order], links_table[Arel.star])
        .select('count(public_bodies.*) as count_of_bodies')
        .joins(:public_body_heading)
        .merge(PublicBodyHeading.by_display_order)
        .joins(public_body_category: :public_bodies)
        .merge(PublicBody.is_requestable)
        .group('has_tag_string_tags.name',
               'public_body_headings.display_order',
               'public_body_category_links.public_body_category_id',
               'public_body_category_links.public_body_heading_id',
               'public_body_category_links.category_display_order',
               'public_body_category_links.id')
        .order(:category_display_order)
        .preload(
          public_body_heading: :translations,
          public_body_category: :translations
        )
    end
  end

end
