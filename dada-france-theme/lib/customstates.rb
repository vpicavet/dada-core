# See `http://alaveteli.org/docs/customising/themes/#customising-the-request-states`
# for more explanation of this file

# Required for zeitwerk loader in rails 7
class Customstates
end

module InfoRequestCustomStates
  def self.included(base)
    base.extend(ClassMethods)
  end

  # Work out what the situation of the request is. In addition to
  # values of self.described_state, in base Alaveteli can return
  # these (calculated) values:
  #   waiting_classification
  #   waiting_response_overdue
  #   waiting_response_very_overdue
  def theme_calculate_status
    # just fall back to the core calculation
    base_calculate_status
  end

  # Mixin methods for InfoRequest
  module ClassMethods
    # Return the name of a custom status.
    def theme_display_status(status)
      raise _('unknown status ') + status unless status == 'saisine_cada'

      _('Saisine CADA en cours')
    end

    # Return the list of custom statuses added by the theme.
    def theme_extra_states
      %w[saisine_cada]
    end

    # extend the model with a method to provide short descriptions for our
    # custom states.
    def theme_short_description(state)
      raise 'Unknown status {{state}}', state: state unless state == 'saisine_cada'

      'Saisine CADA en cours'
    end
  end
end

module RequestControllerCustomStates
  # `theme_describe_state` is called after the core describe_state code.
  # It should end by raising an error if the status is unknown.
  # Example of how to add a custom status:
  # def theme_describe_state(info_request)
  #   if info_request.calculate_status == 'transferred'
  #     flash[:notice] = _("Authority has transferred your request to a different public body.")
  #       redirect_to request_url(@info_request)
  #   else
  #     raise "unknown calculate_status " + info_request.calculate_status
  #   end
  # end
  def theme_describe_state(info_request)
    unless info_request.calculate_status == 'saisine_cada'
      raise 'unknown calculate_status ' + info_request.calculate_status
    end

    flash[:notice] =
      _("Saisine CADA en cours: Ma Dada ne gère pas (encore) la saisine automatiquement. Il vous appartient d'envoyer le document par vous-même.")
    redirect_to request_url(@info_request)
  end
end
