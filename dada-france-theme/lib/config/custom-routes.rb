# -*- encoding : utf-8 -*-
# Here you can override or add to the pages in the core website

Rails.application.routes.draw do
  # brand new controller example
  # get '/mycontroller' => 'general#mycontroller'
  # Additional help page example
  # get '/help/help_out' => 'help#help_out'

  get '/forum' => redirect('https://forum.madada.fr'),
      as: :forum

  get '/stats' => redirect('https://stats.madada.fr/public/dashboard/88dfe578-ca74-4b21-8c46-65259083ce47'),
      as: :stats

  get '/madada_blog' => redirect('https://blog.madada.fr/'),
      as: :madada_blog

  match '/api/v2/tags.json' => 'api#list_tags',
        :as => :api_list_tags,
        :via => :get

  match '/demande/:url_title/tableur_saisine' => 'request#download_batch_saisine_spreadsheet',
        :as => :download_batch_saisine_spreadsheet,
        :via => :get

  match '/request/:url_title/download_saisine' => 'request#download_entire_request_saisine',
        :as => :download_entire_request_saisine,
        :via => :get

  match '/demande/:url_title/saisir_la_cada' => 'request#send_cada_appeal',
        :as => :send_cada_appeal,
        :via => %i[get patch]

  match '/alaveteli_pro/info_requests/:url_title/saisir_la_cada' => 'request#send_cada_appeal',
        :as => :send_cada_appeal_pro,
        :via => %i[get patch]


  # HELP / AIDE
  match '/aide/a_propos' => redirect('https://doc.madada.fr/'),
        :as => :help_about,
        :via => :get
  match '/aide/alaveteli' => redirect('https://doc.madada.fr/'),
        :as => :help_alaveteli,
        :via => :get
  match '/aide/prada' => redirect('https://doc.madada.fr/prada'),
        :as => :help_officers,
        :via => :get
  match '/aide/demandes' => redirect('https://doc.madada.fr/'),
        :as => :help_requesting,
        :via => :get
  match '/aide/confidentialite' => redirect('https://doc.madada.fr/usagers/confidentialite'),
        :as => :help_privacy,
        :via => :get
  match '/aide/credits' => redirect('https://doc.madada.fr/usagers/confidentialite'),
        :as => :help_credits,
        :via => :get
  match '/aide/cada' => redirect('https://doc.madada.fr/prada'),
        :as => :help_cada,
        :via => :get
end
