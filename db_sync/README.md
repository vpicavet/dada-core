# Connecteur entre l'annuaire des PRADA de la CADA et la db Ma Dada

Ces scripts sont executés une fois par nuit pour garder un historique
des versions de l'annuaire de la CADA (scrapé sur le web) et nous
permettre d'effectuer certaines actions sur la base des changements.

Les fichiers produits sont enregistrés ici:
https://gitlab.com/madada-team/annuaire-cada
