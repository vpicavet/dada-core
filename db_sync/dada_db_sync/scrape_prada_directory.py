from datetime import date
from typing import Iterable, NewType

import pandas as pd
import requests
from bs4 import BeautifulSoup

"""
A small scraper which fetches all PRADAs listed in the search results
from https://www.cada.fr/administration/personnes-responsables
This list seems more up to date than the CSV export.

The search results page is enough as it has the same content as
the "details" page, plus better structure.
We add date, and url as a unique id.

The result is saved as CSV on a separate git repo.

The output CSV file is then used by the matching algorithm to link
each record to one of madada.fr's database.
"""


# the categories used by cada.fr, with a random numeric id used
# to build search result urls
PradaCategory = NewType("PradaCategory", str)

prada_categories: dict[PradaCategory, str] = {
    PradaCategory("23470"): "Autorité administrative indépendante",
    PradaCategory("29939"): "Autre organisme privé",
    PradaCategory("29871"): "Collectivité territoriale",
    PradaCategory("29858"): "Conseil départemental",
    PradaCategory("29875"): "Conseil régional",
    PradaCategory("29874"): "Établissement public d'État",
    PradaCategory("29857"): "Établissement public territorial",
    PradaCategory("29855"): "Mairie",
    PradaCategory("29865"): "Organisme privé chargé de la gestion d'un service public",
    PradaCategory("29859"): "Services de l'État",
}

path_prefix = "annuaire_cada"
prada_dir_filename = f"{path_prefix}/annuaire_prada_web.csv"
previous_dir_filename = f"{path_prefix}/hier.csv"
prada_diff_filename = f"{path_prefix}/diff.csv"

session = requests.Session()


def fetch_page_n_for_category(category: PradaCategory, page_num: int) -> str | None:
    """Return the contents of the search results page as str"""
    url = (
        "https://www.cada.fr/administration/personnes-responsables-administration?"
        f"field_classement_de_l_administra_target_id={category}"
        f"&field_d_partement_de_l_autorit__target_id=All&page={page_num}"
    )
    result = session.get(url)
    print(".", sep="", end="", flush=True)
    if "Votre recherche n'a retourné aucun résultat." in result.text:
        return None
    return result.text


def get_pages(category_id: PradaCategory) -> Iterable[str]:
    """Yield each of the search results pages for the given category"""
    page_num: int = 0
    print(
        f"\nScraping category {prada_categories[category_id]}",
        sep="",
        end="",
        flush=True,
    )
    while (
        page_content := fetch_page_n_for_category(category_id, page_num)
    ) is not None:
        yield page_content
        page_num += 1


def get_pradas_in_single_page(page_content: str, category: str) -> Iterable[dict]:
    """Yield all prada records in a given search results page"""
    # As we need categories to be able to match with our dataset, we
    # search https://www.cada.fr/administration/personnes-responsables
    # with each category, and add that to the resulting values.
    soup = BeautifulSoup(page_content, features="html.parser")
    for item in soup.find_all("div", class_="views-row"):
        prada_item = {
            "url_id": item.find("h2", class_="title").a["href"],
            "categorie": category,
            "nom": item.find("h2", class_="title").a.get_text().strip(" \n"),
            "prada": item.find("div", class_="field-name-pr-nom-prada")
            .div.div.get_text()
            .strip(" \n"),
            "adresse": ",".join(
                line
                for line in list(
                    item.find(
                        "div", class_="field-name-field-adresse-complete"
                    ).p.stripped_strings
                )
                if line != "/"  # some addresses are malformed
            ),
            "email": item.find("a", class_="mailto").get_text().strip(" \n"),
        }
        yield prada_item


def get_pradas() -> Iterable[dict]:
    """Yield all prada records for all categories"""
    for category in list(prada_categories.keys()):
        for page in get_pages(category):
            yield from get_pradas_in_single_page(page, prada_categories[category])
    print("")


def dump_prada_directory() -> pd.DataFrame:
    pradas_list = list(get_pradas())
    pradas_df = pd.DataFrame.from_records(pradas_list)
    pradas_df["date"] = date.today()
    pradas_df.sort_values(["categorie", "url_id"])
    pradas_df["cp"] = pradas_df["adresse"].str.extract(
        r",(?P<cp>\d{5}) [^,]*?(?: CEDEX.*)?(?: \d{1,2})?$", expand=True
    )

    # do some manual cleanup that we know of
    pradas_df.loc[
        pradas_df.url_id == "/centre-hospitalier-de-saint-flour", "cp"
    ] = "15100"

    # check that postcodes have the expected format
    invalid_cp = pradas_df[~pradas_df.cp.str.match(r"^\d{5}", na=False)]
    if len(invalid_cp.index) > 0:
        print("Invalid postcodes:")

    pradas_df.to_csv(prada_dir_filename, index=False)
    return pradas_df


def fetch_previous_directory() -> pd.DataFrame:
    """Load yesterday's directory dump from our git repo."""
    # url = "https://gitlab.com/madada-team/annuaire-prada/annuaire_prada_web.csv"
    # the code expects to run in CI, with the previous results cloned locally
    # and correctly named
    try:
        previous_dir = (
            pd.read_csv(previous_dir_filename, dtype="object")
            .drop(columns=["date"])
            .email.fillna("")
        )
    except FileNotFoundError:
        print("Previous dump not found, using empty file")
        previous_dir = pd.DataFrame(
            columns=["url_id", "categorie", "nom", "prada", "adresse", "email", "cp"]
        )
    return previous_dir


def get_df_diff() -> pd.DataFrame:
    prada_directory = dump_prada_directory()
    previous_directory = fetch_previous_directory()

    diff = prada_directory.drop(columns=["date"]).merge(
        # merging on the intersection of columns present in both df
        previous_directory,
        indicator=True,
        how="outer",
    )
    # drop records that did not change
    diff = diff.loc[diff["_merge"] != "both"]
    diff._merge = diff._merge.cat.rename_categories(
        {"right_only": "old", "left_only": "new"}
    )
    print("Diff between yesterday and today")
    print(diff)
    # Records that have `new` in the `_merge` column are either:
    # - a new record if no `old` exists with the same url_id
    # - a modified record if there is another one with the same url_id
    return diff


if __name__ == "__main__":
    df = get_df_diff()
    df.to_csv(prada_diff_filename, index=False)
